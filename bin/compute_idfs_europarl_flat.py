
import crosslingual.utils.idfs as idfs_utils
import numpy as np
import collections
import argparse
import codecs


def get_argument_parser():

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    parser.add_argument('europarl_filepath',
        help='path to the tokenized europarl file',
        type=str)

    parser.add_argument('output_idfs_filepath',
        help='path to the output file to write the idf data',
        type=str)

    parser.add_argument('--subtract_min_idf',
        help='subtract to all the idfs, the minimum idf',
        action='store_true')

    return parser


def read_flat_europarl(europarl_filepath):

    with codecs.open(europarl_filepath, 'r', 'utf-8') as europarl_file:
        europarl_str = europarl_file.read().lower()

        europarl_token_list = europarl_str.replace('\n', ' ').split(' ')

        europarl_document = collections.Counter(europarl_token_list)

    return europarl_document


def compute_flat_idfs(document_dict, subtract_min_idf=False):

    total_tokens = np.float(sum(document_dict.itervalues()))

    idfs_dict = {token : np.log(total_tokens / count)
            for token, count in document_dict.iteritems()}

    # if the translation option is set, the most frequent token is made to have
    # zero idf and the other tokens are translated accordingly
    if subtract_min_idf == True:
        min_idf = min(idfs_dict.itervalues())

        idfs_dict = {token : idf - min_idf
                for token, idf in idfs_dict.iteritems()}

    return idfs_dict


if __name__ == '__main__':

    parser = get_argument_parser()
    args = parser.parse_args()

    # read the file with the europarl data
    europarl_document = read_flat_europarl(args.europarl_filepath)

    # compute and write the idfs to a file
    idfs_dict = compute_flat_idfs(europarl_document, args.subtract_min_idf)
    idfs_utils.write_idfs_to_filename(idfs_dict, args.output_idfs_filepath)


