#!/bin/bash

# NOTE this script you to set some of the variables
# NOTE that in this code I use an unweighted_titov_idfs and unweighted_titov_idfs
# to simulate not using idfs.

# exiting if some error occurs
set -e
bin_folder='bin'

# NOTE I have used the unweighted idfs, when no idfs are used.

# input
reuters_folder="data/rcv-from-binod"
idfs_folder='data/idfs'
embeddings_folder='data/embeddings'
embeddings_name='all_128_blunsom_embeddings_nobad2'
idfs_name='titov_idfs'
noidfs_name='unweighted_blunsom_idfs'

# output
logs_folder="logs/${embeddings_name}"

declare -a dataset_size_array=("100" "200" "500" "1000" "5000" "10000")
declare -a language_array=("en" "de")

### NOTE this part has to be changed later. It is just supposed to be quick to
# see if things are going on well.
declare -a dataset_size_array=("100")
declare -a language_array=("en")

logistic_options='--number_of_regularization_params 50'

# NOTE some are still not used
transfer_options=""
alignments_options=""
transfer_align_gloss_options="--common 1"

transfer_align_source_de_options="--reverse_transfer_direction"
transfer_align_source_en_options=""

# create the folder for output the information if it does not exist already
mkdir -p $logs_folder

# go over all the datasets and generate all the logs
for language in "${language_array[@]}"; do

    # for the transfer case
    # source language
    source_language="${language}"

    # get the target language
    if [ "${language}" == "en" ]; then
        target_language='de'
        transfer_align_options=$transfer_align_source_en_options

    elif [ "${language}" == "de" ]; then
        target_language='en'
        transfer_align_options=$transfer_align_source_de_options
    else
        echo "Unknown language specified (${language}). Exiting."
        exit 0
    fi

    # this is used for the name of the datasets
    language_uppercase=`echo ${language} | tr '[:lower:]' '[:upper:]'`

    for dataset_size in "${dataset_size_array[@]}"; do
        # get the name of the datasets for the supervised experiments
        sup_train_dataset_name="${language_uppercase}${dataset_size}"
        sup_test_dataset_name="${language}"

        # supervised logistic with a embeddings representation (no idfs, blunsom embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/$noidfs_name.${language}" \
            "${embeddings_folder}/${embeddings_name}.${language}" \
            $logistic_options > \
            "$logs_folder/emb_sup_no_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        # supervised logistic with a embeddings representation (idfs, blunsom embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/$idfs_name.${language}" \
            "${embeddings_folder}/${embeddings_name}.${language}" \
            $logistic_options > \
            "$logs_folder/emb_sup_with_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        # get the name of the datasets for the transfer experiments
        transfer_train_dataset_name="${sup_train_dataset_name}"
        transfer_test_dataset_name="${target_language}"

        # transfer using embeddings (with idfs, blunsom)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/$idfs_name.$source_language" \
            "$embeddings_folder/${embeddings_name}.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/$idfs_name.$target_language" \
            "$embeddings_folder/${embeddings_name}.$target_language" \
            $logistic_options > \
            "$logs_folder/emb_transfer_with_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        # transfer using embeddings (no idfs, blunsom)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/$noidfs_name.$source_language" \
            "$embeddings_folder/${embeddings_name}.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/$noidfs_name.$target_language" \
            "$embeddings_folder/${embeddings_name}.$target_language" \
            $logistic_options > \
            "$logs_folder/emb_transfer_no_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" 
    done
done

