import sys
import pdb


# NOTE I could have done a state machine implementation to this but it would
# be more complicated to implement. doing two passes is much simpler.
def bad_lines(filepath_l1, filepath_l2, n):

    with open(filepath_l1, 'r') as f_l1, open(filepath_l2, 'r') as f_l2:

        bad_indices = []
        for i, (line_l1, line_l2) in enumerate(zip(f_l1, f_l2)):

            if line_l1.strip() == "" or line_l2.strip() == "":
                # add a neighbourhood of i as being bad lines
                bad_indices.extend(xrange(i - n, i + n + 1))

        return set(bad_indices)

def clean_file(filepath, bad_indices, outsuffix='.nobadlines'):

    with open(filepath, 'r') as f, open(filepath + outsuffix, 'w') as f_out:
        for i, line in enumerate(f):
            if i not in bad_indices:
                f_out.write(line)


if __name__ == "__main__":
    filepath_l1 = sys.argv[1]
    filepath_l2 = sys.argv[2]
    n = int(sys.argv[3])

    suffix = '.nobadlines_' + str(n)
    bad_indices = bad_lines(filepath_l1, filepath_l2, n)
    clean_file(filepath_l1, bad_indices, suffix)
    clean_file(filepath_l2, bad_indices, suffix)

