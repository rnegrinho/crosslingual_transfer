#!/bin/bash

# exiting if some error occurs
set -e
bin_folder='bin'

# NOTE I may change the idfs that are used here

# input 
reuters_folder="data/rcv-from-binod"
idfs_folder='data/flat_europarl_idfs'
embeddings_folder='data/embeddings'

en_embeddings_filepath="$embeddings_folder/titov_embeddings.en"
de_embeddings_filepath="$embeddings_folder/titov_embeddings.de"
en_de_alignments_filepath='data/english_to_foreign_alignments.counts'

# output 
logs_folder='logs/flat_europarl_idfs_performance'

declare -a dataset_size_array=("100" "200" "500" "1000" "5000" "10000")
declare -a language_array=("en" "de")

#### NOTE, this needs to be changed appropriately.
# It may be necessary to change some names
# this is still not used there
logistic_options='--number_of_regularization_params 50'

# NOTE these are still not used
transfer_options=""
alignments_options=""
transfer_align_gloss_options="--common 1"

transfer_align_source_de_options="--reverse_transfer_direction"
transfer_align_source_en_options=""

# options for using alignments in the transfer part
transfer_align_idfs_options='--source_idfs_filepath 
    \${${source_language}_idfs_filepath} 
    --target_idfs_filepath 
    \${${target_language}_idfs_filepath}' 

# create the folder for output the information if it does not exist already
mkdir -p $logs_folder

# get the list of all idf files
idfs_filename_array=(`ls ${idfs_folder} | sed 's/\(.*\).../\1/g' | uniq`)

# for each idfs filename 
for idfs_filename in "${idfs_filename_array[@]}"; do

    echo "**** Running for $idfs_filename"
    # go over all the datasets and generate all the logs
    for language in "${language_array[@]}"; do

        echo "*** Running for $language "

        # set the idfs files 
        en_idfs_filepath="${idfs_folder}/${idfs_filename}.en"
        de_idfs_filepath="${idfs_folder}/${idfs_filename}.de"

        # for the transfer case
        # source language
        source_language="${language}"

        # get the target language 
        if [ "${language}" == "en" ]; then
            target_language='de'

        elif [ "${language}" == "de" ]; then
            target_language='en'
        else
            echo "Unknown language specified (${language}). Exiting."
            exit 0
        fi

        # this is used for the name of the datasets
        language_uppercase=`echo ${language} | tr '[:lower:]' '[:upper:]'`

        for dataset_size in "${dataset_size_array[@]}"; do
            echo "** The current dataset size is $dataset_size"

            # get the name of the datasets for the supervised experiments
            sup_train_dataset_name="${language_uppercase}${dataset_size}"
            sup_test_dataset_name="${language}"


            echo "* Running the supervised models"

            echo "Logistic regression with bow representation with idfs"
            # supervised logistic with a bag of words representation (with idfs)
            eval eval python "$bin_folder"/run_supervised_logistic_bow.py \
                "$reuters_folder/train/${sup_train_dataset_name}" \
                "$reuters_folder/test/${sup_test_dataset_name}" \
                $logistic_options \
                --idfs_filepath \${${language}_idfs_filepath} > \
                "$logs_folder/bow_sup_${idfs_filename}_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
                &

            echo "Logistic regression with embeddings representation with idfs"
            # supervised logistic with a embeddings representation
            eval eval python "$bin_folder"/run_supervised_logistic_embeddings.py \
                "$reuters_folder/train/${sup_train_dataset_name}" \
                "$reuters_folder/test/${sup_test_dataset_name}" \
                \${${language}_idfs_filepath} \
                \${${language}_embeddings_filepath} \
                $logistic_options > \
                "$logs_folder/emb_sup_${idfs_filename}_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
                &

            # get the name of the datasets for the transfer experiments
            transfer_train_dataset_name="${sup_train_dataset_name}"
            transfer_test_dataset_name="${target_language}"
            transfer_source_test_dataset="${source_language}"

            echo "* Running the transfer models from $source_language to $target_language"

            echo "Transfer with alignments with idfs"
            # transfer experiments
            # simple transfer without processing the alignments (with idfs)
            eval eval python "$bin_folder"/run_transfer_alignments.py \
                "$reuters_folder/train/${transfer_train_dataset_name}" \
                "$reuters_folder/test/${transfer_source_test_dataset}" \
                "$reuters_folder/test/${transfer_test_dataset_name}" \
                $en_de_alignments_filepath \
                "--normalize" \
                $logistic_options \
                $transfer_align_idfs_options \
                \${transfer_align_source_${language}_options} > \
                "$logs_folder/align_transfer_${idfs_filename}_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
                &

            echo "Gloss transfer with idfs"
            # simple transfer with glossed alignments (with idfs)
            eval eval python "$bin_folder"/run_transfer_alignments.py \
                "$reuters_folder/train/${transfer_train_dataset_name}" \
                "$reuters_folder/test/${transfer_source_test_dataset}" \
                "$reuters_folder/test/${transfer_test_dataset_name}" \
                $en_de_alignments_filepath \
                "--normalize" \
                $transfer_align_gloss_options \
                $logistic_options \
                \${transfer_align_source_${language}_options} \
                $transfer_align_idfs_options > \
                "$logs_folder/align_transfer_gloss_${idfs_filename}_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
                &

            echo "Embeddings transfer with idfs"
            # transfer using the titov baseline
            eval eval python "$bin_folder"/run_transfer_embeddings.py \
                "$reuters_folder/train/${transfer_train_dataset_name}" \
                \${${source_language}_idfs_filepath} \
                \${${source_language}_embeddings_filepath} \
                "$reuters_folder/test/${transfer_test_dataset_name}" \
                \${${target_language}_idfs_filepath} \
                \${${target_language}_embeddings_filepath} \
                $logistic_options > \
                "$logs_folder/titov_transfer_${idfs_filename}_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt"
        done
    done
done

