#!/bin/bash

# what do do:
# I have to check how the scripts are called
# I have to guarantee what I'm doing makes sense
# check how idfs can be used in this case (the case for idfs can be dangerous)
# make this run for some correct form (because otherwise, it does not make 
# sense)
# NOTE NOTE*** all the experiments related with running code related with 
# the frank wolfe transfer code should be done here


# NOTE this is script is used to see if the following edge cases are verified

# not using any idfs for now

# NOTE NOTE NOTE
# increase the number of steps to run
# make it use all datasets


# exiting if some error occurs
set -e
bin_folder='bin'

# input 
reuters_folder="data/rcv-from-binod"
en_de_alignments_filepath='data/english_to_foreign_alignments.counts'

# output 
logs_folder='logs/frank_wolfe_transfer'
# this has to be done
rm -rf "$logs_folder"

declare -a dataset_size_array=("100" "200" "500" "1000" "5000" "10000")
declare -a language_array=("en" "de")

# single set of parameters for testing 
declare -a dataset_size_array=("100")
declare -a language_array=("en")

# NOTE this is done only for testing purposes
# some variables to encapsulate some of the options to pass to the scripts
logistic_options='--number_of_regularization_params 20'
alignments_options="--rare_alignments 10 --common 10 --rare_target 2000 --normalize"
transfer_align_source_de_options="--reverse_transfer_direction"
transfer_align_source_en_options=""

# some common options for the alignments transfer using frank wolfe
fw_transfer_options='--fw_transfer_number_of_folds 5 --fw_transfer_number_of_steps 10 --alignments_cost log'

# create the folder for output the information if it does not exist already
mkdir -p $logs_folder

# go over all the datasets and generate all the logs
for language in "${language_array[@]}"; do

    # for the transfer case
    # source language
    source_language="${language}"

    # get the target language 
    if [ "${language}" == "en" ]; then
        target_language='de'

    elif [ "${language}" == "de" ]; then
        target_language='en'
    else
        echo "Unknown language specified (${language}). Exiting."
        exit 0
    fi

    # this is used for the name of the datasets
    upp_source_language=`echo ${source_language} | tr '[:lower:]' '[:upper:]'`
    upp_target_language=`echo ${target_language} | tr '[:lower:]' '[:upper:]'`

    for dataset_size in "${dataset_size_array[@]}"; do

        ## get the name of the datasets for the transfer experiments
        source_train_dataset_name="${upp_source_language}${dataset_size}"
        source_test_dataset_name="${source_language}"
        # NOTE that I'm just using the smaller dataset of the target language
        target_train_dataset_name="${upp_target_language}100"
        target_test_dataset_name="${target_language}"

        # simple transfer with glossed alignments (no idfs)
        eval eval python "$bin_folder"/run_transfer_alignments.py \
            "$reuters_folder/train/${source_train_dataset_name}" \
            "$reuters_folder/test/${target_test_dataset_name}" \
            $en_de_alignments_filepath \
            "--source_test_reuters_directory_path \
            $reuters_folder/test/${source_test_dataset_name}" \
            $alignments_options \
            $logistic_options \
            \${transfer_align_source_${language}_options} > \
            "$logs_folder/align_transfer_${source_train_dataset_name}_${target_test_dataset_name}.txt" \
            &
        
         #simple transfer with frank wolfe (no idfs)
         #high cost in for deviating from gloss. 
         #compare results with above
        eval eval python "$bin_folder"/run_transfer_frank_wolfe.py \
            "$reuters_folder/train/${source_train_dataset_name}" \
            "$reuters_folder/train/${target_train_dataset_name}" \
            "$reuters_folder/test/${target_test_dataset_name}" \
            $en_de_alignments_filepath \
            "--source_test_reuters_directory_path \
            $reuters_folder/test/${source_test_dataset_name}" \
            $alignments_options \
            $logistic_options \
            $fw_transfer_options \
            "--fw_transfer_lower_regularization_param 1e-6 --fw_transfer_upper_regularization_param 1e6 --fw_transfer_number_of_regularization_params 5" \
            \${transfer_align_source_${language}_options} > \
            "$logs_folder/frank_wolfe_transfer_${source_train_dataset_name}_${target_test_dataset_name}.txt" \
            #&

        eval eval python "$bin_folder"/run_transfer_frank_wolfe_reuters_unaligned.py \
            "$reuters_folder/train/${source_train_dataset_name}" \
            "$reuters_folder/train/${target_train_dataset_name}" \
            "$reuters_folder/test/${target_test_dataset_name}" \
            $en_de_alignments_filepath \
            "--source_test_reuters_directory_path \
            $reuters_folder/test/${source_test_dataset_name}" \
            $alignments_options \
            $logistic_options \
            $fw_transfer_options \
            "--aligned_lower_regularization_param 1e-6 --aligned_upper_regularization_param 1e6 --aligned_number_of_regularization_params 5" \
            "--unaligned_lower_regularization_param 1e-3 --unaligned_upper_regularization_param 1e3 --unaligned_number_of_regularization_params 3" \
            \${transfer_align_source_${language}_options} > \
            "$logs_folder/frank_wolfe_transfer_unaligned_${source_train_dataset_name}_${target_test_dataset_name}.txt" \
            #&
    done
done

# equivalence tests for the two frank wolfe transfer algorithms
# ***
# if the regularization constant for the unaligned part is set to zero, this 
# reduces to the simpler frank wolfe transfer case where one ignores the words
# that are not aligned in the europarl

# in the simpler frank wolfe transfer, if we process the alignments matrix 
# such that only the most frequent word is active and all the rest are 
# remove (--common 1), then running the frank wolfe transfer here is equivalent
# to doing a simple gloss transfer


