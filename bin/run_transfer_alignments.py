

import crosslingual.reuters.processing as reuters_proc
import crosslingual.reuters.input_parsing as reuters_input
import crosslingual.bag_of_words.input_parsing as bow_input
import crosslingual.bag_of_words.processing as bow_proc
import crosslingual.alignments.input_parsing as align_input
import crosslingual.alignments.processing as align_proc
import crosslingual.alignments.output_printing as align_output
import crosslingual.logistic.input_parsing as log_input
import crosslingual.logistic.processing as log_proc
import crosslingual.logistic.output_printing as log_ouput
import crosslingual.utils.classification_output as classif_output
import crosslingual.utils.dataset as data_utils
import crosslingual.utils.idfs as idfs_utils
import crosslingual.embeddings.processing as emb_proc
import argparse
import pprint

# NOTE some options may become optional. like testing in domain and
# using a conservative dictionary.

def get_argument_parser():
    """
    Returns the argument parser for the command line input arguments. To get
    the arguments one needs to call the script.
    """

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    reuters_input.set_argument_parser(parser, 'source_')
    reuters_input.set_argument_parser(parser, '--source_test_')
    reuters_input.set_argument_parser(parser, 'target_')
    bow_input.set_argument_parser(parser, 'source_')
    bow_input.set_argument_parser(parser, 'target_')
    align_input.set_argument_parser(parser)
    log_input.set_argument_parser(parser)

    parser.add_argument('--randomize',
        help='shuffle the training and test datasets',
        action='store_true')

    return parser


if __name__ == '__main__':

    # parse the arguments
    parser = get_argument_parser()
    args = parser.parse_args()

    # reads the alignments first
    processing_taglist = align_input.get_processing_taglist_from_input(args)
    align_results = align_proc.load_alignments_information(
        args.english_to_foreign_alignments_filepath, processing_taglist,
        args.reverse_transfer_direction)


    # read the dataset from the directory
    source_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.source_reuters_directory_path)

    source_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(source_dataset_dict)

    # check if there is a specified source idfs file to compute a representation
    # for the documents in the source language
    if args.source_idfs_filepath != None:
        source_idfs_dict = idfs_utils.read_idfs_from_filename(args.source_idfs_filepath)
    else:
        source_idfs_dict = None

    # NOTE In the bag of words representation, I could just use the words that
    # I have seen both in the source reuters training set and in the alignments.

    # get the bag of words representation for the source dataset
    source_token_to_index_dict = align_results['source_token_to_index_dict']
    source_matrix, source_labels = bow_proc.transform_dataset(source_dataset_bow_dict,
        source_token_to_index_dict, args.source_bow_binarize, \
        args.source_bow_normalize, source_idfs_dict)

    if args.randomize == True:
        source_matrix, source_labels = data_utils.randomize_dataset_representation(
            source_matrix, source_labels)

    # gets the best logistic classifier for the information given
    log_results = log_proc.get_best_logistic_classifier(source_matrix, source_labels,
        args.number_of_folds, args.number_of_regularization_params,
        args.lower_regularization_param, args.upper_regularization_param)

    # if there is a source test path defined, used it to evaluate
    if args.source_test_reuters_directory_path != None:
        # get the source test data
        source_test_dataset_dict = reuters_proc.read_dataset_from_directory(
            args.source_test_reuters_directory_path)

        # convert to a bag of words representation
        source_test_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(source_test_dataset_dict)

        # get the bag of words representation for the source dataset
        source_test_matrix, source_test_labels = bow_proc.transform_dataset(
            source_test_dataset_bow_dict, source_token_to_index_dict, \
            args.source_bow_binarize, args.source_bow_normalize, source_idfs_dict)

        # predict the labels for the source test side
        weight_vector = log_results['weight_vector']
        class_list = log_results['class_list']
        pred_source_test_labels = log_proc.predict_labels(weight_vector, class_list, source_test_matrix)

    # get the target test data
    target_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.target_reuters_directory_path)

    target_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(target_dataset_dict)

    # check if there is a specified target idfs file to compute a representation
    # for the documents in the target language
    if args.target_idfs_filepath != None:
        target_idfs_dict = idfs_utils.read_idfs_from_filename(args.target_idfs_filepath)
    else:
        target_idfs_dict = None

    # get a bag of words representation for the test data
    target_token_to_index_dict = align_results['target_token_to_index_dict']

    target_matrix, target_labels = bow_proc.transform_dataset(target_dataset_bow_dict,
        target_token_to_index_dict, args.target_bow_binarize, \
        args.target_bow_normalize, target_idfs_dict)

    # transfer the weight vector
    # NOTE it is assumed that the alignments matrix is returned in the correct
    # direction
    # unpacking the weight_vector and the class_list to pass to the scoring function
    alignments_matrix = align_results['alignments_matrix']
    weight_vector = log_results['weight_vector']
    class_list = log_results['class_list']
    transfered_weight_vector = alignments_matrix.dot(weight_vector)

    # get the predictied labels
    pred_target_labels = log_proc.predict_labels(transfered_weight_vector, class_list, target_matrix)

    # printing of the information about the experiment
    print "**** Information on transfering the model using alignments"
    print "--> Input arguments"
    pprint.pprint(vars(args))
    print "--> Source dataset information"
    classif_output.print_class_label_distribution(source_dataset_dict)
    print "--> Target dataset information"
    classif_output.print_class_label_distribution(target_dataset_dict)
    print "--> Alignments information"
    align_output.print_information(align_results)
    print "--> Training information at the source side"
    log_ouput.print_information(log_results)
    # print if a source test dataset is defined
    if args.source_test_reuters_directory_path != None:
        print "--> Testing information at the source side"
        classif_output.print_error_label_distribution(source_test_labels, pred_source_test_labels)
    print "--> Testing information at the target side"
    classif_output.print_error_label_distribution(target_labels, pred_target_labels)


