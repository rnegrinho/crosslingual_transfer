
import crosslingual.embeddings.processing as emb_proc
import crosslingual.alignments.processing as align_proc
import crosslingual.reuters.processing as reuters_proc
import crosslingual.bag_of_words.processing as bow_proc
import crosslingual.utils.idfs as idfs_utils

# note that all the information that is read already comes in the lower case
# format, so there is no need to convert it. things are directly comparable.
if __name__ == "__main__":

    # path for the idfs for both languages
    en_idfs_filename = 'data/idfs/titov_idfs.en'
    de_idfs_filename = 'data/idfs/titov_idfs.de'

    # reading the idfs for both languages
    en_idfs_dict = idfs_utils.read_idfs_from_filename(en_idfs_filename)
    de_idfs_dict = idfs_utils.read_idfs_from_filename(de_idfs_filename)

    # reading the alignments
    english_to_foreign_alignments_filepath = 'data/english_to_foreign_alignments.counts'

    alignments_dict = \
        align_proc.read_alignments_file(english_to_foreign_alignments_filepath)

    english_token_dict, foreign_token_dict = \
        align_proc.compute_mappings_token_to_index(alignments_dict)

    # reading the reuters training part
    en_dataset_directory_path = 'data/rcv-from-binod/train/EN10000'
    de_dataset_directory_path = 'data/rcv-from-binod/train/DE10000'

    # reading the datasets for both languages
    en_reuters_dataset_dict = \
        reuters_proc.read_dataset_from_directory(en_dataset_directory_path)

    de_reuters_dataset_dict = \
        reuters_proc.read_dataset_from_directory(de_dataset_directory_path)

    # convert both datasets to a bow representation
    en_reuters_bow_dataset_dict = \
        reuters_proc.convert_dataset_to_bow(en_reuters_dataset_dict)

    de_reuters_bow_dataset_dict = \
        reuters_proc.convert_dataset_to_bow(de_reuters_dataset_dict)

    # compute the mappings from tokens to indices for both languages
    english_reuters_token_dict = \
        bow_proc.compute_token_to_index_dict(en_reuters_bow_dataset_dict, 1)

    foreign_reuters_token_dict = \
        bow_proc.compute_token_to_index_dict(de_reuters_bow_dataset_dict, 1)

    # convert everything to sets
    en_idfs_tokens = set(en_idfs_dict.keys())
    de_idfs_tokens = set(de_idfs_dict.keys())

    en_align_tokens = set(english_token_dict.keys())
    de_align_tokens = set(foreign_token_dict.keys())

    en_reuters_tokens = set(english_reuters_token_dict.keys())
    de_reuters_tokens = set(foreign_reuters_token_dict.keys())

    # check the sizes of the number of tokens in each thing
    print "For the english tokens:"
    print "Titov's: ", len(en_idfs_tokens)
    print "Europarl: ", len(en_align_tokens)
    print "Reuters: ", len(en_reuters_tokens)
    print "Intersection with Europarl: ", \
        len(en_idfs_tokens.intersection(en_align_tokens))
    print "Intersection with Reuters: ", \
        len(en_idfs_tokens.intersection(en_reuters_tokens))

    print "For the foreign tokens:"
    print "Titov's: ", len(de_idfs_tokens)
    print "Europarl: ", len(de_align_tokens)
    print "Reuters: ", len(de_reuters_tokens)
    print "Intersection with Europarl: ", \
        len(de_idfs_tokens.intersection(de_align_tokens))
    print "Intersection with Reuters: ", \
        len(de_idfs_tokens.intersection(de_reuters_tokens))



