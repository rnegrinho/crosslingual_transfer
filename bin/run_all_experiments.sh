#!/bin/bash

# exiting if some error occurs
set -e

# NOTE I have used the unweighted idfs, when no idfs are used.
bin_folder='bin'

# input 
reuters_folder="data/rcv-from-binod"
idfs_folder='data/idfs'
embeddings_folder='data/embeddings'
en_de_alignments_filepath='data/english_to_foreign_alignments.counts'
titov_embeddings_name='titov_embeddings'
blunsom_embeddings_name='all_128_blunsom_embeddings_nobad2'


# output 
logs_folder='logs/all_experiments'

declare -a dataset_size_array=("100" "200" "500" "1000" "5000" "10000")
declare -a language_array=("en" "de")

declare -a dataset_size_array=("100")
declare -a language_array=("en" "de")

logistic_options='--number_of_regularization_params 50'

# NOTE some are still not used
transfer_options=""
alignments_options=""
transfer_align_gloss_options="--common 1"

transfer_align_source_de_options="--reverse_transfer_direction"
transfer_align_source_en_options=""
            
# create the folder for output the information if it does not exist already
mkdir -p $logs_folder

# go over all the datasets and generate all the logs
for language in "${language_array[@]}"; do

    echo "*** Running the experiments for $language"

    # for the transfer case
    # source language
    source_language="${language}"

    # get the target language 
    if [ "${language}" == "en" ]; then
        target_language='de'
        transfer_align_options=$transfer_align_source_en_options

    elif [ "${language}" == "de" ]; then
        target_language='en'
        transfer_align_options=$transfer_align_source_de_options
    else
        echo "Unknown language specified (${language}). Exiting."
        exit 0
    fi

    # this is used for the name of the datasets
    language_uppercase=`echo ${language} | tr '[:lower:]' '[:upper:]'`

    for dataset_size in "${dataset_size_array[@]}"; do

        echo "** The current dataset size is $dataset_size"

        # get the name of the datasets for the supervised experiments
        sup_train_dataset_name="${language_uppercase}${dataset_size}"
        sup_test_dataset_name="${language}"

        echo "* Running the supervised models"

        echo "Logistic regression with bow representation and no idfs"
        ## *** supervised methods ***
        ## supervised logistic with a bag of words representation (no idfs)
        python "$bin_folder"/run_supervised_logistic_bow.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            $logistic_options \
            --idfs_filepath "${idfs_folder}/unweighted_titov_idfs.${language}" > \
            "$logs_folder/bow_sup_no_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &
        
        echo "Logistic regression with bow representation with idfs"
        # supervised logistic with a bag of words representation (with idfs)
        python "$bin_folder"/run_supervised_logistic_bow.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            $logistic_options \
            --idfs_filepath "${idfs_folder}/titov_idfs.${language}" > \
            "$logs_folder/bow_sup_with_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        echo "Logistic regression with Titov embeddings representation and no idfs"
        # supervised logistic with a embeddings representation (no idfs, titov embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/unweighted_titov_idfs.${language}" \
            "${embeddings_folder}/$titov_embeddings_name.${language}" \
            $logistic_options > \
            "$logs_folder/titov_emb_sup_no_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        echo "Logistic regression with Blunsom embeddings representation and no idfs"
        # supervised logistic with a embeddings representation (no idfs, blunsom embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/unweighted_titov_idfs.${language}" \
            "${embeddings_folder}/$blunsom_embeddings_name.${language}" \
            $logistic_options > \
            "$logs_folder/blunsom_emb_sup_no_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        echo "Logistic regression with Titov embeddings representation with idfs"
        # supervised logistic with a embeddings representation (idfs, titov embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/titov_idfs.${language}" \
            "${embeddings_folder}/$titov_embeddings_name.${language}" \
            $logistic_options > \
            "$logs_folder/titov_emb_sup_with_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \
            &

        echo "Logistic regression with Blunsom embeddings representation with idfs"
        # supervised logistic with a embeddings representation (idfs, blunsom embeddings)
        python "$bin_folder"/run_supervised_logistic_embeddings.py \
            "$reuters_folder/train/${sup_train_dataset_name}" \
            "$reuters_folder/test/${sup_test_dataset_name}" \
            "${idfs_folder}/titov_idfs.${language}" \
            "${embeddings_folder}/$blunsom_embeddings_name.${language}" \
            $logistic_options > \
            "$logs_folder/blunsom_emb_sup_with_idfs_${sup_train_dataset_name}_${sup_test_dataset_name}.txt" \

        # get the name of the datasets for the transfer experiments
        transfer_train_dataset_name="${sup_train_dataset_name}"
        transfer_test_dataset_name="${target_language}"

        echo "* Running the transfer models from $source_language to $target_language"

        echo "Transfer with alignments and no idfs"
        ### *** transfer experiments ***
        # simple transfer without processing the alignments (no idfs)
        python "$bin_folder"/run_transfer_alignments.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            $en_de_alignments_filepath \
            "--normalize" \
            $logistic_options \
            $transfer_align_options \
            --source_idfs_filepath "$idfs_folder/unweighted_titov_idfs.$source_language" \
            --target_idfs_filepath "$idfs_folder/unweighted_titov_idfs.$target_language" > \
            "$logs_folder/align_transfer_no_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        echo "Transfer with alignments and idfs"
        # simple transfer without processing the alignments (with idfs)
        python "$bin_folder"/run_transfer_alignments.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            $en_de_alignments_filepath \
            "--normalize" \
            $logistic_options \
            $transfer_align_options \
            --source_idfs_filepath "$idfs_folder/titov_idfs.$source_language" \
            --target_idfs_filepath "$idfs_folder/titov_idfs.$target_language" > \
            "$logs_folder/align_transfer_with_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        echo "Gloss transfer without idfs"
        # simple transfer with glossed alignments (no idfs)
        python "$bin_folder"/run_transfer_alignments.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            $en_de_alignments_filepath \
            $transfer_align_gloss_options \
            "--normalize" \
            $logistic_options \
            $transfer_align_options \
            --source_idfs_filepath "$idfs_folder/unweighted_titov_idfs.$source_language" \
            --target_idfs_filepath "$idfs_folder/unweighted_titov_idfs.$target_language" > \
            "$logs_folder/align_transfer_gloss_no_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        echo "Gloss transfer with idfs"
        # simple transfer with glossed alignments (with idfs)
        python "$bin_folder"/run_transfer_alignments.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            $en_de_alignments_filepath \
            $transfer_align_gloss_options \
            "--normalize" \
            $logistic_options \
            $transfer_align_options \
            --source_idfs_filepath "$idfs_folder/titov_idfs.$source_language" \
            --target_idfs_filepath "$idfs_folder/titov_idfs.$target_language" > \
            "$logs_folder/align_transfer_gloss_with_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            

        echo "Titov embeddings transfer without idfs"
         #this the part of transfering with embeddings
         #transfer using embeddings (no idfs, titov)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/unweighted_titov_idfs.$source_language" \
            "$embeddings_folder/$titov_embeddings_name.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/unweighted_titov_idfs.$target_language" \
            "$embeddings_folder/$titov_embeddings_name.$target_language" \
            $logistic_options > \
            "$logs_folder/titov_transfer_no_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            

        echo "Titov embeddings transfer with idfs"
        # transfer using embeddings (with idfs, titov)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/titov_idfs.$source_language" \
            "$embeddings_folder/$titov_embeddings_name.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/titov_idfs.$target_language" \
            "$embeddings_folder/$titov_embeddings_name.$target_language" \
            $logistic_options > \
            "$logs_folder/titov_transfer_with_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        echo "Blunsom embeddings transfer without idfs"
        # transfer using embeddings (no idfs, blunsom)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/unweighted_titov_idfs.$source_language" \
            "$embeddings_folder/$blunsom_embeddings_name.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/unweighted_titov_idfs.$target_language" \
            "$embeddings_folder/$blunsom_embeddings_name.$target_language" \
            $logistic_options > \
            "$logs_folder/blunsom_transfer_no_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt" \
            &

        echo "Blunsom embeddings transfer with idfs"
        # transfer using embeddings (with idfs, blunsom)
        python "$bin_folder"/run_transfer_embeddings.py \
            "$reuters_folder/train/${transfer_train_dataset_name}" \
            "$idfs_folder/titov_idfs.$source_language" \
            "$embeddings_folder/$blunsom_embeddings_name.$source_language" \
            "$reuters_folder/test/${transfer_test_dataset_name}" \
            "$idfs_folder/titov_idfs.$target_language" \
            "$embeddings_folder/$blunsom_embeddings_name.$target_language" \
            $logistic_options > \
            "$logs_folder/blunsom_transfer_with_idfs_${transfer_train_dataset_name}_${transfer_test_dataset_name}.txt"
    done
done

