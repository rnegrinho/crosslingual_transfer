
# NOTE this is code that will probably need to be profiled
# NOTE it may be needed to do some output in the transfer using the training
# NOTE there is a need to take the auxiliary information out of the frank wolfe
# algorithm
# NOTE add an option to the processing of the alignments matrix that just
# keeps the sparsity pattern making every alignment equally costly.
# NOTE add an option to the processing of alignments matrix that binarizes the
# matrix of alignments
# NOTE see how poor is the result of the computation (this can be done in terms
# of the duality gap)
# NOTE there are a lot of functions that I'm using from the logistic module
# but this is not good semantics because that code was developed solely for
# the logistic regression part. A better solution would be to check what code
# from the logistic module I am using and factorizing it in some set of common
# utils
# NOTE how to deal with the words in the training set that are unaligned but
# appear in the target training data
# NOTE it needs to do some validation with the normal transfer: if the
# regularization is set too high, it is just like doing the normal transfer doing
# NOTE there are questions on how this formulation can be relaxed. what
# would be an appropriate formualation. some constraints seem too strong, but
# I believe it may be interesting to consider other approaches

import crosslingual.reuters.processing as reuters_proc
import crosslingual.reuters.input_parsing as reuters_input
import crosslingual.bag_of_words.input_parsing as bow_input
import crosslingual.bag_of_words.processing as bow_proc
import crosslingual.alignments.input_parsing as align_input
import crosslingual.alignments.processing as align_proc
import crosslingual.alignments.output_printing as align_output
import crosslingual.logistic.input_parsing as log_input
import crosslingual.logistic.processing as log_proc
import crosslingual.logistic.output_printing as log_ouput
import crosslingual.utils.classification_output as classif_output
import crosslingual.utils.dataset as data_utils
import crosslingual.utils.idfs as idfs_utils
import crosslingual.embeddings.processing as emb_proc
import crosslingual.learners.frank_wolfe_transfer as fw_trans
import numpy as np
import argparse
import pprint


def get_argument_parser():
    """
    Returns the argument parser for the command line input arguments. To get
    the arguments one needs to call the script.
    """

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    reuters_input.set_argument_parser(parser, 'source_train_')
    reuters_input.set_argument_parser(parser, '--source_test_')
    reuters_input.set_argument_parser(parser, 'target_train_')
    reuters_input.set_argument_parser(parser, 'target_test_')
    bow_input.set_argument_parser(parser, 'source_')
    bow_input.set_argument_parser(parser, 'target_')
    align_input.set_argument_parser(parser)
    log_input.set_argument_parser(parser)

    parser.add_argument('--randomize',
        help='shuffle the training and test datasets',
        action='store_true')

    # this is the part related with the frank wolfe
    parser.add_argument('--fw_transfer_number_of_steps',
        help='number of steps of the Frank Wolfe algorithm',
        type=int,
        default=100)

    # it has to be one of the
    parser.add_argument('--alignments_cost',
        help='cost function derived from the alignments',
        type=str,
        default='log')

    # NOTE this part is justified by the fact that log_input has the same options
    # as we need for the frank wolfe transfer.
    # this is not a good programming practice. ideally, things should be captured
    # in common functions instead of using something that is not connected
    # with frank wolfe or replicating the code here.
    log_input.set_argument_parser(parser, 'fw_transfer_')

    return parser


# reads the dataset directly from the path file
def read_dataset_in_matrix_representation(reuters_directory_path,
    token_to_index_dict, bow_binarize, bow_normalize, idfs_dict=None):

    # read the dataset from the directory
    reuters_dataset_dict = reuters_proc.read_dataset_from_directory(
        reuters_directory_path)

    reuters_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(
        reuters_dataset_dict)

    # get the bag of words representation for the source dataset
    data_matrix, data_labels = bow_proc.transform_dataset(
        reuters_dataset_bow_dict, token_to_index_dict,
        bow_binarize, bow_normalize, idfs_dict)

    return data_matrix, data_labels


# NOTE this probably should go to a separate file. this is too cluttered here.
if __name__ == '__main__':

    ### parse the arguments
    parser = get_argument_parser()
    args = parser.parse_args()

    ### reads the alignments
    processing_taglist = align_input.get_processing_taglist_from_input(args)
    align_results = align_proc.load_alignments_information(
        args.english_to_foreign_alignments_filepath, processing_taglist,
        args.reverse_transfer_direction)

    # get the dictionaries from the alignments information
    source_token_to_index_dict = align_results['source_token_to_index_dict']

    # source idfs
    if args.source_idfs_filepath != None:
        source_idfs_dict = idfs_utils.read_idfs_from_filename(args.source_idfs_filepath)
    else:
        source_idfs_dict = None

    ### train dataset
    source_train_matrix, source_train_labels = read_dataset_in_matrix_representation(
        args.source_train_reuters_directory_path, source_token_to_index_dict,
        args.source_bow_binarize, args.source_bow_normalize, source_idfs_dict)

    if args.randomize == True:
        source_train_matrix, source_train_labels = \
            data_utils.randomize_dataset_representation(source_train_matrix, source_train_labels)

    # gets the best logistic classifier for the information given
    log_results = log_proc.get_best_logistic_classifier(source_train_matrix,
        source_train_labels, args.number_of_folds, args.number_of_regularization_params,
        args.lower_regularization_param, args.upper_regularization_param)


    # if there is a source test path defined, used it to evaluate
    if args.source_test_reuters_directory_path != None:
        # get the source test data
        source_test_matrix, source_test_labels = read_dataset_in_matrix_representation(
        args.source_test_reuters_directory_path, source_token_to_index_dict,
        args.source_bow_binarize, args.source_bow_normalize, source_idfs_dict)

        # predict the labels for the source test side
        weight_vector = log_results['weight_vector']
        class_list = log_results['class_list']
        pred_source_test_labels = log_proc.predict_labels(weight_vector, class_list, source_test_matrix)

    # this part is the part related with the transfer of the model according to
    # the frank wolfe formulation

    ### load the target training data and train the model in the target side
    target_token_to_index_dict = align_results['target_token_to_index_dict']

    # target idfs
    if args.target_idfs_filepath != None:
        target_idfs_dict = idfs_utils.read_idfs_from_filename(args.target_idfs_filepath)
    else:
        target_idfs_dict = None

    # get the target train data
    target_train_matrix, target_train_labels = read_dataset_in_matrix_representation(
        args.target_train_reuters_directory_path, target_token_to_index_dict,
        args.target_bow_binarize, args.target_bow_normalize, target_idfs_dict)

    # transfer the weight vector
    # NOTE it is assumed that the alignments matrix is returned in the correct
    # direction
    # even the onehot latels can be done there
    alignments_matrix = align_results['alignments_matrix']
    source_weight_vector = log_results['weight_vector']
    class_list = log_results['class_list']

    # compute a cost alignments matrix
    cost_alignments_matrix = align_proc.compute_cost_matrix_from_alignments(alignments_matrix,
        args.alignments_cost)

    # calling the function that gets the best model
    fw_weight_vector, fw_alignments_matrix = fw_trans.fit_model(
        target_train_matrix, target_train_labels, cost_alignments_matrix,
        alignments_matrix, source_weight_vector, class_list,
        args.fw_transfer_lower_regularization_param,
        args.fw_transfer_upper_regularization_param,
        args.fw_transfer_number_of_regularization_params,
        args.fw_transfer_number_of_folds,
        args.fw_transfer_number_of_steps)

    # evaluate the model in the target dataset
    target_test_matrix, target_test_labels = read_dataset_in_matrix_representation(
    args.target_test_reuters_directory_path, target_token_to_index_dict,
    args.target_bow_binarize, args.target_bow_normalize, target_idfs_dict)

    # get the predictied labels
    pred_target_test_labels = log_proc.predict_labels(fw_weight_vector, class_list,
        target_test_matrix)

    ### output target data
    # NOTE I may add the information about the distributions of the labels for
    # the source test case

    # printing of the information about the experiment
    # NOTE see how these things work at the source and at the target side
    # especially with all the divisions between things
    print "**** Information on transfering the model using alignments"
    print "--> Input arguments"
    pprint.pprint(vars(args))
    # NOTE this information is commented out for now, but it needs to be put
    # out later. NOTE maybe use something that just depends on the labels.
    print "--> Source train dataset information"
    classif_output.print_label_distribution(source_train_labels)
    print "--> Target train dataset information"
    classif_output.print_label_distribution(target_train_labels)
    print "--> Target test dataset information"
    classif_output.print_label_distribution(target_test_labels)
    print "--> Alignments information"
    align_output.print_information(align_results)
    print "--> Training information at the source side"
    log_ouput.print_information(log_results)
    # print if a source test dataset is defined
    if args.source_test_reuters_directory_path != None:
        print "--> Testing information at the source side"
        classif_output.print_error_label_distribution(source_test_labels,
            pred_source_test_labels)

    print "--> Testing information at the target side"
    classif_output.print_error_label_distribution(target_test_labels,
        pred_target_test_labels)



