

import crosslingual.reuters.processing as reuters_proc
import crosslingual.reuters.input_parsing as reuters_input
import crosslingual.embeddings.input_parsing as emb_input
import crosslingual.embeddings.processing as emb_proc
import crosslingual.logistic.input_parsing as log_input
import crosslingual.logistic.processing as log_proc
import crosslingual.logistic.output_printing as log_ouput
import crosslingual.utils.classification_output as classif_output
import crosslingual.utils.dataset as data_utils
import crosslingual.utils.idfs as idfs_utils
import argparse
import pprint

# NOTE there may exist some problems with the lack of an intercept term

def get_argument_parser():
    """
    Returns the argument parser for the command line input arguments. To get
    the arguments one needs to call the script.
    """

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    reuters_input.set_argument_parser(parser, 'train_')
    reuters_input.set_argument_parser(parser, 'test_')
    emb_input.set_argument_parser(parser)
    log_input.set_argument_parser(parser)

    parser.add_argument('--randomize',
        help='shuffle the training and test datasets',
        action='store_true')

    return parser


if __name__ == '__main__':

    # parse the arguments
    parser = get_argument_parser()
    args = parser.parse_args()

    # read the dataset from the directory
    train_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.train_reuters_directory_path)

    train_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(train_dataset_dict)

    # get an embeddings representation for the documents
    idfs_dict = idfs_utils.read_idfs_from_filename(args.idfs_filepath)
    embeddings_dict = emb_proc.read_embeddings_from_file(args.embeddings_filepath)

    train_matrix, train_labels = emb_proc.transform_dataset(train_dataset_bow_dict,
        idfs_dict, embeddings_dict, args.emb_binarize, args.emb_normalize)

    if args.randomize == True:
        train_matrix, train_labels = data_utils.randomize_dataset_representation(
            train_matrix, train_labels)

    # gets the best logistic classifier for the information given
    log_results = log_proc.get_best_logistic_classifier(train_matrix, train_labels,
        args.number_of_folds, args.number_of_regularization_params,
        args.lower_regularization_param, args.upper_regularization_param)

    # unpacking the weight_vector and the class_list to pass to the scoring function
    weight_vector = log_results['weight_vector']
    class_list = log_results['class_list']

    # get the test data
    # read the dataset from the directory
    test_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.test_reuters_directory_path)

    test_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(test_dataset_dict)

    test_matrix, test_labels = emb_proc.transform_dataset(test_dataset_bow_dict,
        idfs_dict, embeddings_dict, args.emb_binarize, args.emb_normalize)

    # get the predictied labels
    pred_test_labels = log_proc.predict_labels(weight_vector, class_list, test_matrix)

    # printing of the information about the experiment
    print "**** Information on the logistic supervised using embeddings"
    print "--> Input arguments"
    pprint.pprint(vars(args))
    print "--> Training set information"
    classif_output.print_class_label_distribution(train_dataset_dict)
    print "--> Test set information"
    classif_output.print_class_label_distribution(test_dataset_dict)
    print "--> Training information"
    log_ouput.print_information(log_results)
    print "--> Testing information"
    classif_output.print_error_label_distribution(test_labels, pred_test_labels)

