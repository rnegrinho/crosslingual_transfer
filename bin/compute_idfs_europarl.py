
import crosslingual.utils.idfs as idfs_utils
import collections
import argparse
import codecs


def get_argument_parser():

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    parser.add_argument('europarl_filepath',
        help='path to the tokenized europarl file',
        type=str)

    parser.add_argument('output_idfs_filepath',
        help='path to the output file to write the idf data',
        type=str)

    parser.add_argument('number_of_sentences_per_document',
        help='how many consecutive sentences to use to make a document',
        type=int)

    parser.add_argument('--frequency_threshold',
        help='sets to zero the idfs of the words that have frequency above the \
            specified threshold',
        type=float,
        default=1.0)

    # some more arguments for the processing may go here

    return parser


def read_europarl(europarl_filepath, nsent_per_doc):

    assert nsent_per_doc >= 1, (
       "The number of sentences to use per document has to be greater than zero.")

    europarl_file = codecs.open(europarl_filepath, 'r', 'utf-8')

    document_list = list()
    document = collections.Counter()
    for n, line in enumerate(europarl_file):

        line_token_list = line.lower().rstrip(' \n').split()
        document.update(line_token_list)

        # if the number of sentences to aggregate is reached, commit the document
        # to the list
        if (n + 1) % nsent_per_doc == 0:
            document_list.append(document)
            document = collections.Counter()

    return document_list


if __name__ == '__main__':

    parser = get_argument_parser()
    args = parser.parse_args()

    # read the file with the europarl data
    europarl_document_list = read_europarl(args.europarl_filepath,
        args.number_of_sentences_per_document)

    # compute and write the idfs to a file
    idfs_dict = idfs_utils.compute_idfs(europarl_document_list, args.frequency_threshold)
    idfs_utils.write_idfs_to_filename(idfs_dict, args.output_idfs_filepath)


