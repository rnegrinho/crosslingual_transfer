

import crosslingual.reuters.processing as reuters_proc
import crosslingual.reuters.input_parsing as reuters_input
import crosslingual.embeddings.input_parsing as emb_input
import crosslingual.embeddings.processing as emb_proc
import crosslingual.logistic.input_parsing as log_input
import crosslingual.logistic.processing as log_proc
import crosslingual.logistic.output_printing as log_ouput
import crosslingual.utils.classification_output as classif_output
import crosslingual.utils.dataset as data_utils
import crosslingual.utils.idfs as idfs_utils
import argparse
import pprint


def get_argument_parser():
    """
    Returns the argument parser for the command line input arguments. To get
    the arguments one needs to call the script.
    """

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    reuters_input.set_argument_parser(parser, 'source_')
    emb_input.set_argument_parser(parser, 'source_')
    reuters_input.set_argument_parser(parser, 'target_')
    emb_input.set_argument_parser(parser, 'target_')
    log_input.set_argument_parser(parser)

    parser.add_argument('--randomize',
        help='shuffle the training and test datasets',
        action='store_true')

    return parser


if __name__ == '__main__':

    # parse the arguments
    parser = get_argument_parser()
    args = parser.parse_args()

    # read the dataset from the directory
    source_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.source_reuters_directory_path)

    source_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(source_dataset_dict)

    # get an embeddings representation for the documents
    source_idfs_dict = idfs_utils.read_idfs_from_filename(args.source_idfs_filepath)
    source_embeddings_dict = emb_proc.read_embeddings_from_file(args.source_embeddings_filepath)

    source_matrix, source_labels = emb_proc.transform_dataset(source_dataset_bow_dict,
        source_idfs_dict, source_embeddings_dict, args.source_emb_binarize, args.source_emb_normalize)

    if args.randomize == True:
        source_matrix, source_labels = data_utils.randomize_dataset_representation(
            source_matrix, source_labels)

    # gets the best logistic classifier for the information given
    log_results = log_proc.get_best_logistic_classifier(source_matrix, source_labels,
        args.number_of_folds, args.number_of_regularization_params,
        args.lower_regularization_param, args.upper_regularization_param)

    # unpacking the weight_vector and the class_list to pass to the scoring function
    weight_vector = log_results['weight_vector']
    class_list = log_results['class_list']

    # get the test data
    # read the dataset from the directory
    target_dataset_dict = reuters_proc.read_dataset_from_directory(
        args.target_reuters_directory_path)

    target_dataset_bow_dict = reuters_proc.convert_dataset_to_bow(target_dataset_dict)

    # get an embeddings representation for the documents
    target_idfs_dict = idfs_utils.read_idfs_from_filename(args.target_idfs_filepath)
    target_embeddings_dict = emb_proc.read_embeddings_from_file(args.target_embeddings_filepath)

    target_matrix, target_labels = emb_proc.transform_dataset(target_dataset_bow_dict,
        target_idfs_dict, target_embeddings_dict, args.target_emb_binarize, args.target_emb_normalize)

    # get the predictied labels
    pred_target_labels = log_proc.predict_labels(weight_vector, class_list, target_matrix)

    # printing of the information about the experiment
    print "**** Information on transfering the model using embeddings and keeping the weight vector"
    print "--> Input arguments"
    pprint.pprint(vars(args))
    print "--> Source dataset information"
    classif_output.print_class_label_distribution(source_dataset_dict)
    print "--> Target dataset information"
    classif_output.print_class_label_distribution(target_dataset_dict)
    print "--> Training information at the source side"
    log_ouput.print_information(log_results)
    print "--> Testing information at the target side"
    classif_output.print_error_label_distribution(target_labels, pred_target_labels)

