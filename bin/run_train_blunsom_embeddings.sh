#!/bin/bash

# NOTE running this script requires the code from Karl Moritz to train his 
# embeddings. It also requires that the path to the files mentioned are correct.

set -e

# these input files need to be changed
corpus_name='europarl_en_de_tokenized_lowercased_nobadlines_2'
in1='europarl-v7.de-en.tokenized.lowercased.en.nobadlines_2'
in2='europarl-v7.de-en.tokenized.lowercased.de.nobadlines_2'
corpus_path="data/"

#declare -a noise_array=("1" "10" "50")
#declare -a step_size_array=("0.01" "0.05")
#declare -a mini_batch_size_array=("10" "50")

declare -a noise_array=("10")
declare -a step_size_array=("0.05")
declare -a mini_batch_size_array=("10")

echo "Training the with the following input files:"
echo "$in1"
echo "$in2"

# embeddings training
for noise in "${noise_array[@]}"; do
    for step_size in "${step_size_array[@]}"; do
        for mini_batch_size in "${mini_batch_size_array[@]}"; do

            echo "Training with noise ($noise), step size ($step_size), and mini batch size ($mini_batch_size)"

            out_path="data/${corpus_name}_noise_${noise}_stepsize_${step_size}_minibatchsize_${mini_batch_size}"

            # make the path if it does not exist
            mkdir -p $out_path

            ./bin/dbltrain --input1 "$corpus_path/$in1" \
                --input2 "$corpus_path/$in2" \
                --tree plain \
                --type additive \
                --method adagrad \
                --word-width 128 \
                --hinge_loss_margin 128 \
                --model1-out "$out_path/${in1}.model" \
                --model2-out "$out_path/${in2}.model" \
                --noise $noise \
                --batches $mini_batch_size \
                --eta $step_size \
                --lambdaD 1 \
                --calc_bi_error1 true \
                --calc_bi_error2 true \
                --iterations 100 >& $out_path/log.txt &
        done
    done

    wait

    echo "Finished training with noise ($noise) and all possible remaining parameters"

    for step_size in "${step_size_array[@]}"; do
        for mini_batch_size in "${mini_batch_size_array[@]}"; do
            echo "Extracting embeddings with noise ($noise), step size ($step_size), and mini batch size ($mini_batch_size)"

            out_path="data/${corpus_name}_noise_${noise}_stepsize_${step_size}_minibatchsize_${mini_batch_size}"

            ./bin/extract-vectors -m "${out_path}/${in1}.model" > "$out_path/${in1}.embeddings" &
            ./bin/extract-vectors -m "${out_path}/${in2}.model" > "$out_path/${in2}.embeddings" &
            done
    done

    wait

    echo "Finished extracting with noise ($noise) and all possible remaining parameters"
done

