
set -e
bin_folder='bin'

# this is used to run the computation of the idfs
# paralellize every four processes

en_europarl="data/europarl-v7.de-en.tokenized.en"
de_europarl="data/europarl-v7.de-en.tokenized.de"
en_reuters="data/rcv-from-binod/train/EN10000"
de_reuters="data/rcv-from-binod/train/DE10000"

idfs_output_folder="data/idfs/"

# create the folder for the computed idfs
mkdir -p $idfs_output_folder

declare -a frequency_threshold_array=("1.0" "0.75")
declare -a sentence_per_document_array=("1" "5" "10")
declare -a language_array=("en" "de")

#for the europarl dataset for both languages
for language in "${language_array[@]}"; do

    # the document based europarl idfs
    for n_sentences in "${sentence_per_document_array[@]}"; do
        for threshold in "${frequency_threshold_array[@]}"; do
            eval python "$bin_folder"/compute_idfs_europarl.py \${${language}_europarl} \
                "$idfs_output_folder/europarl_idfs_s${n_sentences}_f${threshold}.${language}" \
                ${n_sentences} --frequency_threshold ${threshold}
        done
    done

    # the flat europarl idfs
    # with no subtraction of the min idf
    eval python "$bin_folder"/compute_idfs_europarl_flat.py \${${language}_europarl} \
        "$idfs_output_folder/europarl_flat_idfs.${language}"

    # with subtraction of the min idf
    eval python "$bin_folder"/compute_idfs_europarl_flat.py \${${language}_europarl} \
        "$idfs_output_folder/europarl_flat_idfs.${language}" \
        --subtract_min_idf
done

# for the reuters dataset for both languages
for language in "${language_array[@]}"; do
    for threshold in "${frequency_threshold_array[@]}"; do
        eval python "$bin_folder"/compute_idfs_reuters.py \${${language}_reuters} \
            "$idfs_output_folder/reuters_idfs_f${threshold}.${language}" \
            --frequency_threshold ${threshold}
    done
done



