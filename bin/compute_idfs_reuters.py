
import crosslingual.utils.idfs as idfs_utils
import crosslingual.reuters.processing as reuters_proc
import argparse


def get_argument_parser():

    # defining the input arguments to be parsed
    parser = argparse.ArgumentParser()

    parser.add_argument('reuters_directory_path',
        help='path to the directory with the reuters data',
        type=str)

    parser.add_argument('output_idfs_filepath',
        help='path to the output file to write the idf data',
        type=str)

    parser.add_argument('--frequency_threshold',
        help='sets to zero the idfs of the words that have frequency above the specified threshold',
        type=float,
        default=1.0)

    # some more arguments for the processing may go here

    return parser


if __name__ == '__main__':

    parser = get_argument_parser()
    args = parser.parse_args()

    # read the file with the europarl data
    reuters_dataset = \
        reuters_proc.read_dataset_from_directory(args.reuters_directory_path)
    reuters_bow_dataset = reuters_proc.convert_dataset_to_bow(reuters_dataset)

    # convert to a single list of documents
    reuters_document_list = list()
    for class_document_list in reuters_bow_dataset.itervalues():
        reuters_document_list.extend(class_document_list)

    # compute and write the idfs to a file
    idfs_dict = idfs_utils.compute_idfs(reuters_document_list, args.frequency_threshold)
    idfs_utils.write_idfs_to_filename(idfs_dict, args.output_idfs_filepath)


