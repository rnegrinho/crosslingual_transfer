
import numpy as np
import collections as cols
import codecs
import os


def read_example_from_filename(example_path):
    """
    Reads an example document and maps it to a dictionary representation of it,
    where each key is a token and the corresponding value is the number of times
    the token appears. The example file has some set of tokens separated by
    spaces, possibilily across different lines.
    """

    with codecs.open(example_path, 'r', 'utf-8') as example_file:
        example = example_file.read().lower()

    return example


def read_examples_from_directory(directory_path):
    """
    Reads all the examples from some specified directory. A list of examples
    represented as dictionaries (the same format as was described in the function
    read_example_from_filename).
    """

    #print directory_path
    # getting just the paths for the non directory files
    example_filepath_list = list()
    for filename in os.listdir(directory_path):
        # concatenating the filename with the path directory
        filepath = os.path.join(directory_path, filename)
        # if it is a file (and not a directory), save the path to a list
        if os.path.isfile(filepath):
            example_filepath_list.append(filepath)

    # for each example (a news) file in the directory
    example_list = list()
    for example_filepath in example_filepath_list:
        example = read_example_from_filename(example_filepath)
        example_list.append(example)

    return example_list


def read_dataset_from_directory(directory_path):
    """
    Reads all the examples from some specified directory. The specified directory
    has inside it some set of subfolders, having each subfolder the examples
    corresponding to that class. Returns a dictionary where each key is
    the name of one class (the name of the corresponding subdirectory) and
    the corresponding value is the list of examples for that class (read from
    the corresponding subdirectory)
    """

    # getting just the paths for the non directory files
    class_directory_path_list = list()
    class_name_list = list()
    for filename in os.listdir(directory_path):
        # concatenating the filename with the path directory
        filepath = os.path.join(directory_path, filename)
        # if it is a directory (and not a file), save the path to a list
        if os.path.isdir(filepath):
            class_directory_path_list.append(filepath)
            class_name_list.append(filename)

    # dataset indexed by the number of classes
    dataset = dict()
    for class_directory_path, class_name in zip(class_directory_path_list, class_name_list):
        dataset[class_name] = read_examples_from_directory(class_directory_path)

    return dataset


def convert_example_to_bow(example_str):
    """
    Converts from the full text string to a bag a words representation for
    that example.
    """
    token_list = example_str.replace('\n', ' ').split(' ')
    example_bow_dict = cols.Counter(token_list)

    return example_bow_dict


def convert_dataset_to_bow(dataset_dict):
    """
    Converts the dataset to a bag of words representation. Returns the new
    representation for the dataset.
    """

    dataset_bow_dict = dict()
    for class_label in dataset_dict:

        class_example_bow_list = [convert_example_to_bow(example_str)
            for example_str in dataset_dict[class_label]]

        dataset_bow_dict[class_label] = class_example_bow_list

    return dataset_bow_dict


