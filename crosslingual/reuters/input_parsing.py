

def set_argument_parser(parser, prefix):

    parser.add_argument(prefix + 'reuters_directory_path',
        help='path to the directory with reuters data',
        type=str)

