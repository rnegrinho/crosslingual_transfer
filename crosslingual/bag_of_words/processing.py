
import scipy.sparse as sp
import numpy as np
import collections


def compute_token_to_index_dict(dataset_bow_dict, count_cutoff):
    """
    Computes the token to index dictionary given a dataset represented as
    a dictionary of examples.
    """

    # sum all the examples to get the total number of tokens
    sum_counter = collections.Counter()
    for class_label in dataset_bow_dict:
        for example_bow_dict in dataset_bow_dict[class_label]:
            sum_counter.update(example_bow_dict)

    # get the token to index mapping according to the count threshold
    valid_tokens = [token for token in sum_counter if sum_counter[token] >= count_cutoff]
    token_to_index = dict(zip(valid_tokens, range(len(valid_tokens))))

    return token_to_index


def transform_example(example_bow_dict, token_to_index, binarize, normalize,
    idfs_dict=None):
    """
    Given an example, which is represented as dictionary of tokens with the
    corresponding counts, and a dictionary that gives the mapping from tokens
    to coordinate indices, returns the vector representation of that example
    as a sparse row vector with the same dimension of the vocabulary. If the
    binarize option is set, the representation is a binary vector instead of
    a vector of counts. If an idfs dict is specified it is computed an
    idf representation instead.
    """

    n_tokens = len(token_to_index)
    example_rep = sp.dok_matrix((1, n_tokens))

    sum_counts = 0.0
    for token in example_bow_dict:
        if token not in token_to_index:
            continue

        # binarize or leave as is
        if binarize:
            count = 1.0
        else:
            count = np.float(example_bow_dict[token])

        # if a dictionary of idfs is passed as argument, compute an idf
        # representation instead
        token_i = token_to_index[token]

        if idfs_dict == None:
            example_rep[0, token_i] = count
            sum_counts += np.float(count)
        else:
            if token in idfs_dict:
                idf = idfs_dict[token]

                example_rep[0, token_i] = count * idf
                sum_counts += np.float(count)

    # convert to csr and normalize if specified
    csr_example_rep = example_rep.tocsr()

    if normalize == True:
        csr_example_rep /= np.float(sum_counts)

    return csr_example_rep


def transform_dataset(dataset_bow_dict, token_to_index, binarize, normalize,
    idfs_dict=None):
    """
    Transforms the dictionary with the several classes into a sparse matrix
    where each row is an example, and a vector of labels that tells what is
    the class of each of the rows. If the binarize option is set, the examples
    are represented by binary vectors instead of count vectors. If the
    randomize options is set, the examples are shuffled around before being
    returned.
    """
    sorted_class_label_list = dataset_bow_dict.keys()
    sorted_class_label_list.sort()

    example_list = list()
    label_list = list()
    for class_label in sorted_class_label_list:
        for example_bow_dict in dataset_bow_dict[class_label]:
            example_rep = transform_example(example_bow_dict, token_to_index,
                binarize, normalize, idfs_dict)

            example_list.append(example_rep)
            label_list.append(class_label)

    data_matrix = sp.vstack(example_list, format='csr')
    data_labels = np.array(label_list, dtype='str')

    return (data_matrix, data_labels)


