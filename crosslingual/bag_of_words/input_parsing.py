
def set_argument_parser(parser, prefix=''):

    # these are the optional arguments
    parser.add_argument('--' + prefix + 'bow_binarize',
        help='binarize the bag of words representation for the documents',
        action='store_true')

    parser.add_argument('--' + prefix + 'bow_normalize',
        help='normalize the bag of words representation for the documents by \
            dividing by the number of existing tokens',
        action='store_true')

    parser.add_argument('--' + prefix + 'idfs_filepath',
        help='path to the file with the idfs. if no file is specified, a \
             simple bag of words representation is used',
        type=str)

