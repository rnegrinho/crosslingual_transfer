
# note that this is going to be used to read both sides of the parallel corpora


def set_argument_parser(parser, prefix):

    parser.add_argument(prefix + 'europarl_filepath',
        help='path to the europarl file',
        type=str)

