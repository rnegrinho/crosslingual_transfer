
import collections as cols
import itertools
import codecs

def read_europarl_file(europarl_filepath, nsentences=None):

    with codecs.open(europarl_filepath, 'r', 'utf-8') as europarl_file:

        if nsentences == None:
            return europarl_file.readlines()
        else:
            return list(itertools.islice(europarl_file, nsentences))

def convert_europarl_to_bow(europarl_sentence_list):
    """
    Converts the europarl sentences to a bag of words representation. Returns the new
    representation.
    """
    europarl_sentence_bow_list = \
        map(convert_example_to_bow, europarl_sentence_list)

    return europarl_sentence_bow_list


def convert_example_to_bow(example_str):

    token_list = example_str.rstrip(' \n').split(' ')
    example_bow_dict = cols.Counter(token_list)

    return example_bow_dict


def extract_mapping(example_bow_list):
    """
    Computes a mapping (i.e., dictionary) from tokens to indices, given a list
    of sentences represented by a bag of words representation.
    """
    tokens = {}
    for example_bow in example_bow_list:
        tokens.update(example_bow)

    sorted_tokens = sorted(list(tokens))
    indices = range(len(tokens))
    mapping = dict(zip(sorted_tokens, indices))

    return mapping

