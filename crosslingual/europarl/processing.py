
import collections as cols
import codecs


# NOTE that the representation in terms of vectors can be done using
# the function that is in the bag of words representation
# something that may be interesting is to remove some of the punctuation

# automatically lowers the text and returns a list of sentences
def read_europarl_file(europarl_filepath):

    europarl_sentence_list = list()
    with codecs.open(europarl_filepath, 'r', 'utf-8') as europarl_file:
        for line in europarl_file:
            # convert to lower
            lower_line = line.lower()
            europarl_sentence_list.append(lower_line)

    return europarl_sentence_list


def convert_europarl_to_bow(europarl_sentence_list):
    """
    Converts the europarl sentences to a bag of words representation. Returns the new
    representation.
    """
    europarl_sentence_bow_list = \
        map(convert_example_to_bow, europarl_sentence_list)

    return europarl_sentence_bow_list


def convert_example_to_bow(example_str):
    """
    Converts from the full text string to a bag a words representation for
    that example.
    """
    token_list = example_str.replace('\n', ' ').split(' ')
    example_bow_dict = cols.Counter(token_list)

    return example_bow_dict

