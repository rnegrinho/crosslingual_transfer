
import numpy as np
import scipy.sparse as sp

# to develop, to compute a coocurrences matrix

# actually, I like functions better than object code, because they have much
# clearer semantics. you insert the data and you get the result (i.e. the
# processed data).

# there is a target direction implied. you convert things in the target language
# to something in the source language, which you can manage

# A - MB
# for each source token, what are the tokens that co-occur with it.

# working directly with matrices is more cost effective


def compute_cooccurrences():
    pass



def extract_sparsity_pattern(sparse_matrix):
    nnz = sparse_matrix.nnz
    shape = sparse_matrix.shape
    indices = sparse_matrix.nonzero()
    values = np.ones(nnz, dtype=float)
    mat = sparse_matrix.tocsr((values, indices), shape)

    return mat









