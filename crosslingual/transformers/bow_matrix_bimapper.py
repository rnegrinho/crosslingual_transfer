
# this import is not correct and I am sure that it can be improved
import crosslingual.utils.bimapping as bm
import scipy.sparse as sp
import itertools

class BowMatrixBiMapper:

    def __init__(self, mapping, size=None):
        self.bimap = bm.bimapping()

        if len(mapping) > 0:
            mn = min(mapping.itervalues())
            if mn < 0:
                raise ValueError("The provided mapping has values negative values (e.g. %d)." % (mn,))

            mx = max(mapping.itervalues())
            if size == None:
                self.size = mx + 1
            else:
                self.size = size
                if self.size <= mx:
                    raise ValueError("The provided mapping has values larger than the specified size (e.g. %d)." % (mx,))
        else:
            self.size = 0

        try:
            self.bimap.direct_extend(mapping.items())
        except bm.InvalidItemError:
            raise ValueError("The provided mapping is not one-to-one (i.e., a bijection).")


    # each row is an example in the new format.
    def convert_from_bow_to_matrix(self, bow_list, ignore=False, nodefault=False, default=None):

        if ignore and not nodefault:
            if default == None:
                raise ValueError("The ignore option is set, nonetheless, a valid default index was not provided.")
            elif default < 0:
                raise ValueError("The default value %s is negative." % (default, ))
            elif default >= self.size:
                raise ValueError("The default value %s falls outside the range of the representation." % (default, ))
            elif self.bimap.inverse_has_key(default):
                raise ValueError("The default value %s index already exists in the mapping." % (default, ))

        mat = sp.dok_matrix((len(bow_list), self.size), dtype='float')
        for i, bow in enumerate(bow_list):
            items = bow.items()
            if len(items) == 0:
                continue

            words, counts = zip(*items)
            indices = self.bimap.direct_apply(words, ignore=ignore, default=None)

            # add just the good indices
            for j, c in itertools.izip(indices, counts):
                if j != None:
                    mat[i, j] += c
                else:
                    if not nodefault:
                        mat[i, default] += c

        return mat.tocsr()

    def convert_from_matrix_to_bow(self, rep_matrix, ignore=False, nodefault=False, default=None):

        if ignore and not nodefault:
            if self.bimap.direct_has_key(default):
                raise ValueError("The default value %s already exists in the mapping." % (default, ))

        bow_list = []
        for row in rep_matrix:
            _, indices = row.nonzero()
            counts = row.data

            # if no tokens, it is just an empty example
            if len(counts) == 0:
                bow_list.append({})
                continue

            # filter the out of domain items in case the options are set
            if ignore and not nodefault:
                f = lambda it: self.bimap.inverse_has_key(it[0])
                indomain_pairs = filter(f, itertools.izip(indices, counts))
                indices, counts = zip(*indomain_pairs)

            words = self.bimap.inverse_apply(indices, ignore=ignore, default=default, nodefault=nodefault)

            bow = {}
            for w, c in itertools.izip(words, counts):
                bow[w] = bow.get(w, 0) + c

            bow_list.append(bow)

        return bow_list


