

def tune_step(x, s, cost_function, update_function):
    beta = 0.25
    alpha = 1.0

    best_alpha = 0.0
    best_cost = np.inf
    while True:
        xtest = update_function(x, s, alpha)
        cost = cost_function(xtest)

        # if it is best than the current point
        if cost < best_cost:
            best_alpha = alpha
            best_cost = cost
            alpha *= beta
        else:
            break

    return best_alpha


def frank_wolfe_optimizer(x0, cost_function, gradient_function, update_function,
    minimum_atom_function, duality_gap_function, nsteps, step_tuning=False):

    xk = x0
    for k in xrange(nsteps):
        xk_grad = gradient_function(xk)
        s = minimum_atom_function(xk_grad)

        if step_tuning:
            alphak = tune_step(xk, s, cost_function, update_function)
        else:
            alphak = 2.0 / (2.0 + k)

        print "step %d, cost %0.5e, duality gap %0.5e, step size %0.5e" % (
            k, cost_function(xk), duality_gap_function(xk_grad, xk, s), alphak)

        xk = update_function(xk, s, alphak)

    return xk

