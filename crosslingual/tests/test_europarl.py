
import crosslingual.europarl.sentence_loader as europarl
import unittest
import os

test_data_folderpath = os.path.join('data', 'test')

class TestEuroparlLoader(unittest.TestCase):

    def test_empty(self):
        empty_path = os.path.join(test_data_folderpath, 'empty.en')
        sentences = europarl.read_europarl_file(empty_path)

        self.assertEqual(len(sentences), 0)
        bows = europarl.convert_europarl_to_bow(sentences)
        self.assertEqual(bows, [])

    def test_three(self):
        three_path = os.path.join(test_data_folderpath, 'three.en')
        sentences = europarl.read_europarl_file(three_path, 10)

        self.assertEqual(len(sentences), 3)

        ref_bows = [{'a' : 1}, {'x' : 1}, {'c' : 1}]
        bows = europarl.convert_europarl_to_bow(sentences)
        self.assertEqual(bows, ref_bows)

        ref_mapping = {'a' : 0, 'c' : 1, 'x' : 2}
        mapping = europarl.extract_mapping(bows)
        self.assertEqual(mapping, ref_mapping)

    def test_sample(self):
        sample_path = os.path.join(test_data_folderpath, 'sample.en')
        sentences = europarl.read_europarl_file(sample_path)

        self.assertEqual(len(sentences), 100)
        bows = europarl.convert_europarl_to_bow(sentences)
        mapping = europarl.extract_mapping(bows)

    def test_read10(self):
        sample_path = os.path.join(test_data_folderpath, 'sample.en')
        ref_sentences = europarl.read_europarl_file(sample_path)
        sentences = europarl.read_europarl_file(sample_path, nsentences=10)

        self.assertEqual(len(sentences), 10)
        self.assertEqual(sentences, ref_sentences[:10])

if __name__ == '__main__':
    unittest.main()

