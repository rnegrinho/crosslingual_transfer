
from crosslingual.utils.bimapping import *
import unittest

class TestBiMapping(unittest.TestCase):


    def test(self):
        m = bimapping()

        self.assertEqual(len(m), 0)
        self.assertEqual(len(list(m.direct_iteritems())), 0)
        self.assertEqual(len(list(m.inverse_iteritems())), 0)

        xs = range(10)
        ys = map(lambda x: x * x, xs)
        items = zip(xs, ys)
        m.direct_extend(items)

        self.assertEqual(len(m), 10)
        self.assertTrue(m.direct_has_key(0))
        self.assertEqual(set(m.direct.itervalues()), set(ys))
        self.assertEqual(set(m.direct.iterkeys()), set(xs))
        self.assertEqual(set(m.inverse.itervalues()), set(xs))
        self.assertEqual(set(m.inverse.iterkeys()), set(ys))

        vs = m.direct_apply(xs)
        self.assertEqual(vs, ys)
        inv_vs = m.inverse_apply(vs)
        self.assertEqual(inv_vs, xs)

        try:
            m.direct_extend([(2, 4)])
            self.assertTrue(False)
        except InvalidItemError:
            pass

        try:
            m.inverse_extend([(4, 2)])
            self.assertTrue(False)
        except InvalidItemError:
            pass

        m.direct_remove([0])
        self.assertFalse(m.direct_has_key(0))
        self.assertFalse(m.inverse_has_key(0))

        self.assertEqual(len(m), 9)
        self.assertEqual(len(list(m.direct_iteritems())), 9)
        self.assertEqual(len(list(m.inverse_iteritems())), 9)
        try:
            m.direct_apply([0])
            self.assertTrue(False)
        except KeyError:
            pass

        vs = m.direct_apply(xs, ignore=True, nodefault=True)
        self.assertEqual(len(vs), 9)

        vs = m.direct_apply(xs, ignore=True)
        self.assertEqual(len(vs), 10)
        self.assertIn(None, vs)

        try:
            m.direct_apply([0])
            self.assertTrue(False)
        except KeyError:
            pass

        inv_vs = m.inverse_apply(vs[1:])
        self.assertEqual(inv_vs, xs[1:])

        m.direct_remove(xs, ignore=True)
        self.assertEqual(len(m), 0)

        vs = m.direct_apply([1, 2, 3], ignore=True, nodefault=True)
        self.assertEqual(vs, [])


if __name__ == '__main__':
    unittest.main()


