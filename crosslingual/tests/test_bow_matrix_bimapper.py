
import unittest
import crosslingual.transformers.bow_matrix_bimapper as bimap
import scipy.sparse as sp
import numpy as np

# this is the code to test the transfer between representations in the bag of words format and representation in the sparse matrix format (think if I can do this


# NOTE: check the InvalidItemException. I am having trouble
# using it in the tests. There is the possibility of changing it
# to a simple value error, but I think it does not capture
# capture the semantics of the situation.

class TestBowMatrixBiMapper(unittest.TestCase):

    def test_empty(self):
        mapping = {}
        mapper = bimap.BowMatrixBiMapper(mapping)

        # check how this works in the case of empty mappings
        bows = []
        mat = mapper.convert_from_bow_to_matrix(bows, ignore=True, nodefault=True)
        self.assertEqual(mat.shape, (0, 0))

    def test_invalid_mapping(self):

        mapping = {1 : 1, 2 : 1}
        self.assertRaises(ValueError, bimap.BowMatrixBiMapper, mapping)

    def test_empty_mapping(self):
        mapping = {}
        mapper = bimap.BowMatrixBiMapper(mapping)
        # check how this works in the case of empty mappings
        bows = [{'a' : 1, 'b' : 2}, {}, {'c' : 1}, {}]
        mat = mapper.convert_from_bow_to_matrix(bows, ignore=True, nodefault=True)
        self.assertEqual(mat.shape, (4, 0))

        # should return an key error if the ignore option is not set
        self.assertRaises(KeyError, mapper.convert_from_bow_to_matrix, bows, ignore=False)

        # should complain about the default value not being valid
        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True)

        # should complain about the default value not being valid
        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True, default=0)

        # should complain about the default value not being valid
        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True, default=-1)

    def test_normal(self):
        mapping = {'a' : 1, 'b' : 2, 'c': 3}
        mapper = bimap.BowMatrixBiMapper(mapping)
        # check how this works in the case of empty mappings
        bows = [{'a' : 1, 'b' : 2, 'd' : 5, 'f' : 2}, {'f' : 1}, {'c' : 1, 'a' : 3, 'g' : 4}, {}]
        mat = mapper.convert_from_bow_to_matrix(bows, ignore=True, default=0)
        self.assertEqual(mat.shape, (4, 4))
        ref_mat = np.array([[7, 1, 2, 0], [1, 0, 0, 0], [4, 3, 0, 1], [0, 0, 0, 0]])
        self.assertTrue(np.all(mat.todense().astype(int) == ref_mat))

        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True, default=1)


    def test_all_indomain(self):
        mapping = {'a' : 0, 'b' : 1, 'c': 2, 'd' : 3}
        mapper = bimap.BowMatrixBiMapper(mapping)
        bows = [{'a' : 1, 'b' : 2}, {'a' : 1}, {'c' : 1, 'a' : 3, 'd' : 4}, {}]

        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True, default=0)

        self.assertRaises(ValueError, mapper.convert_from_bow_to_matrix, bows, ignore=True, default=4)

        mat = mapper.convert_from_bow_to_matrix(bows)
        ref_mat = sp.csr_matrix([[1, 2, 0, 0], [1, 0, 0, 0], [3, 0, 1, 4], [0, 0, 0, 0]])
        self.assertTrue(np.all(mat.todense() == ref_mat.todense()))

        inv_bows = mapper.convert_from_matrix_to_bow(mat)
        self.assertEqual(inv_bows, bows)

        ref_inv_bows = mapper.convert_from_matrix_to_bow(ref_mat)
        self.assertEqual(ref_inv_bows, bows)

    def test_empty_examples(self):
        mapping = {}
        mapper = bimap.BowMatrixBiMapper(mapping)
        ref_bows = [{}, {}, {}]

        mat = mapper.convert_from_bow_to_matrix(ref_bows, ignore=True, nodefault=True)
        self.assertEqual(mat.shape, (3, 0))

        bows = mapper.convert_from_matrix_to_bow(mat)
        self.assertEqual(bows, ref_bows)

if __name__ == '__main__':
    unittest.main()

