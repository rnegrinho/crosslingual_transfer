
def set_argument_parser(parser, prefix=''):
    """
    Sets the parser to parse the typical parameters required for running the
    logistic model. These include the number of folds of cross validation to
    run, the number of regularization parameters to consider and the range of
    the regularization parameters.
    """

    # number of validation folds
    parser.add_argument('--' + prefix + 'number_of_folds',
            help='number of folds used in cross validation to choose the model',
            type=int,
            default=10)

    # number of regularization parameters
    parser.add_argument('--' + prefix + 'number_of_regularization_params',
            help='number of regularization parameters to consider in model selection',
            type=int,
            default=10)

    # lower regularization parametere (higher regularization)
    parser.add_argument('--' + prefix + 'lower_regularization_param',
            help='lower regularization parameter',
            type=float,
            default=1e-6)

    # higher regularization parametere (lower regularization)
    parser.add_argument('--' + prefix + 'upper_regularization_param',
            help='upper regularization parameter',
            type=float,
            default=1e6)

