#!/usr/bin/python

import sklearn.linear_model as lm
import sklearn.cross_validation as cv
import numpy as np


def run_kfold_crossvalidation(logistic_model, train_matrix, train_labels, number_of_folds):
    """
    Given a logistic regression model and a training set of examples, runs
    k-fold cross-validation for the specified number of folds. Returns the average
    performance on all the folds
    """
    strat_kfolder = cv.StratifiedKFold(train_labels, number_of_folds)

    # for each fold of the training data
    score_list = list()
    for train_fold_index, test_fold_index in strat_kfolder:
        # train partition
        train_fold_matrix = train_matrix[train_fold_index]
        train_fold_labels = train_labels[train_fold_index]
        # test partition
        test_fold_matrix = train_matrix[test_fold_index]
        test_fold_labels = train_labels[test_fold_index]

        # train the model in training fold
        logistic_model.fit(train_fold_matrix, train_fold_labels)

        # score the fitted model on the test partition
        fold_score = logistic_model.score(test_fold_matrix, test_fold_labels)
        score_list.append(fold_score)

    return np.mean(score_list)


def get_best_regularization_parameter(logistic_model, train_matrix, train_labels,
                                      number_of_folds, lower_reg_param, upper_reg_param,
                                      number_of_params):
    """
    Given a logistic regression model and a training set of examples, returns
    the best regularization parameter. The best regularization parameter is
    determined using cross validation, where we need to specify the number of
    folds, the number of parameters to test and the interval where the parameter
    lies (the number of parameters is sampled in a logarithmic fashion between
    the lower and higher regularization parameters.)
    """

    log_lower = np.log10(lower_reg_param)
    log_upper = np.log10(upper_reg_param)
    reg_param_list = np.logspace(log_lower, log_upper, number_of_params)

    # best params for the model that we are training
    best_param = None
    best_score = 0.0
    for reg_param in reg_param_list:
        logistic_model.C = reg_param

        # get the score for this regularization parameter
        score = run_kfold_crossvalidation(
                    logistic_model, train_matrix, train_labels, number_of_folds)

        # if the score is better than the score so far, keep it
        if score > best_score:
            best_score = score
            best_param = reg_param

    return (best_param, best_score)


def score_model(weight_vector, class_list, test_matrix, test_labels):
    """
    Outputs the accuracy of the model on the test data. Gets as input the
    weight vectors for each class, the correspondence between the weight
    vectors and the class labels, and the test data in the form of a matrix of
    examples and the corresponding labels.
    """

    # computes the scores of each training example belonging to every class
    scores = test_matrix.dot(weight_vector)

    # compute the predicted labels (i.e., the label with maximum score)
    label_index = np.argmax(scores, axis=1)
    pred_labels = class_list[label_index]

    # compare with the actual labels and determine the accuracy
    number_examples = len(test_labels)
    accuracy = (1.0 / number_examples) * np.sum(test_labels == pred_labels)

    return accuracy


# NOTE this may need to return a numpy array of strings
def predict_labels(weight_vector, class_list, data_matrix):
    """
    Outputs the accuracy of the model on the test data. Gets as input the
    weight vectors for each class, the correspondence between the weight
    vectors and the class labels, and the test data in the form of a matrix of
    examples and the corresponding labels.
    """

    # computes the scores of each example for each class
    scores = data_matrix.dot(weight_vector)

    # compute the predicted labels (i.e., the label with maximum score)
    label_index = np.argmax(scores, axis=1)
    pred_labels = class_list[label_index]

    return np.array(pred_labels)

# get just the information for determining the best classifier given the
# training data and some cross validation scheme.
def get_best_logistic_classifier(train_matrix, train_labels,
                                 number_of_folds,
                                 number_of_regularization_params,
                                 lower_regularization_param,
                                 upper_regularization_param):
    """
    Returns the weight vector of the logistic model for the training data
    passed as input. As input, we pass the training examples and the
    corresponding labels, the number of folds to run cross validation on,
    the number of regularization parameters to try, and the lower and upper
    regularization parameters values for the regularization parameters to try
    (note that a higher regularizaton parameter regularizes less than a
    lower one). Also returns the correspondence between the weight vectors and
    the class labels, the best regularization parameter, the average
    score obtained in cross validation, and the training score. All
    information is returned in the form of a dictionary.
    """

    # logistic regression model. note that there is not intercept for now.
    logistic_model = lm.LogisticRegression(fit_intercept=False)

    # get the best regularization parameter
    best_param, best_score = get_best_regularization_parameter(
                    logistic_model, train_matrix, train_labels, number_of_folds,
                    lower_regularization_param, upper_regularization_param,
                    number_of_regularization_params)

    # fit model using the best parameter setting
    logistic_model.C = best_param
    logistic_model.fit(train_matrix, train_labels)

    # vector with the coefficients of the linear model.
    # each column is the weight vector for a class
    weight_vector = logistic_model.raw_coef_.T.copy()
    # getting information about the classes
    class_list = logistic_model.classes_.copy()
    # evaluate the model on the training set
    train_score = score_model(weight_vector, class_list, train_matrix, train_labels)

    # dictionary to hold the output information
    info_dict = dict()
    info_dict['weight_vector'] = weight_vector
    info_dict['class_list'] = class_list
    info_dict['best_param'] = best_param
    info_dict['best_score'] = best_score
    info_dict['train_score'] = train_score

    # use the results dict to pass the input information too
    info_dict['train_matrix'] = train_matrix
    info_dict['train_labels'] = train_labels
    info_dict['number_of_folds'] = number_of_folds
    info_dict['lower_regularization_param'] = lower_regularization_param
    info_dict['upper_regularization_param'] = upper_regularization_param
    info_dict['number_of_regularization_params'] = number_of_regularization_params

    return info_dict

