

import collections

# this is the printing of the logistic information. I'll assume that the
# dictionary is always full, i.e., that somehow, always the same function is
# called. (no key misses)

def print_information(info_dict):
    """
    Prints the information resulting from running the logistic baseline.
    This prints the information regarding the settings used to train the
    logistic model and the obtained cross-validation and training scores.
    """

    print "** Information about the logistic model"

    # can also add information about the distribution of the classes in
    # the data (i.e., how many classes are there in each case)

    print "Dimension of the weight vector: ", \
        info_dict['train_matrix'].shape[1]

    print "Number of training examples: ", \
        info_dict['train_matrix'].shape[0]

    # computing the distribution of examples among the classes
    class_labels_dist = collections.Counter(info_dict['train_labels'])
    print "Label distribution for the training examples: ", \
        dict(class_labels_dist)

    print "Model selected with %d folds" % \
        info_dict['number_of_folds']

    print "Number of regularization constants: ", \
        info_dict['number_of_regularization_params']

    print "Lower regularization constant: %0.2e" % \
        info_dict['lower_regularization_param']

    print "Upper regularization constant: %0.2e" % \
        info_dict['upper_regularization_param']

    print "Best regularization parameter C found by cross validation: %0.2e" % \
        info_dict['best_param']

    print "Average score for the %d folds: %0.4f" % \
        (info_dict['number_of_folds'], info_dict['best_score'])

    print "Score on the training set: %0.4f" % \
        info_dict['train_score']

    print '** End of the information about the logistic model'
    # dictionary to hold the output information

