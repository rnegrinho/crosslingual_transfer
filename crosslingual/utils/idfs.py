
import codecs
import collections
import numpy as np


# NOTE
# I can have a function here that computes the representation for the
# dataset given a function to transform examples. this has to be done with some
# care, but it can perfectly be done and it may make sense

# it may be necessary to be careful about the type of the data. for example
# the labels may be something other than a string.

# a list of documents where each document is a dictionary of tokens
# NOTE I may need to use a default dict for the idfs_dict

# it may be possible to have a module with idf related functionalities
# the computation of the idfs can be done before using them
# use a lot of options here, with smoothing, with truncation of the most
# frequent words. some filtering and some smoothing can be done here.

def compute_idfs(document_list, frequency_threshold=1.0):

    counts_dict = collections.Counter()

    # compute the number of documents each token occurs in
    for document_dict in document_list:
        for token in document_dict:
            counts_dict[token] += 1.0

    n_documents = np.float(len(document_list))

    # compute the idf
    idfs_dict = dict()
    for token, count in counts_dict.iteritems():
        # frequency threshold
        if (count / n_documents) <= frequency_threshold:
            idf = np.log(n_documents / count)
        else:
            idf = 0.0

        idfs_dict[token] = idf

    return idfs_dict

def read_idfs_from_filename(idfs_filename):
    """
    Reads the idfs from the idf file provided by Ivan Titov. Outputs a
    dictionary where each key is a token and the value is the corresponding idf.
    """

    idfs_file = codecs.open(idfs_filename, 'r', 'utf-8')

    idfs_dict = dict()
    for line in idfs_file:
        line = line.rstrip(' \n')

        # ignoring count for now. just need the idf
        token, idf = line.split('\t')
        idfs_dict[token] = np.float(idf)

    idfs_file.close()

    return idfs_dict

# this how things are going to be written to file
def write_idfs_to_filename(idfs_dict, idfs_filename):

    idfs_file = codecs.open(idfs_filename, 'w', 'utf-8')

    for token, idf in idfs_dict.iteritems():

        idf_line = '%s\t%s\n' % (token, idf)
        idfs_file.write(idf_line)

    idfs_file.close()


