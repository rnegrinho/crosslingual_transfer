
import numpy as np
import scipy.sparse as sp


# permutes the dataset representation arround
def randomize_dataset_representation(data_matrix, data_labels):

    n_examples = data_matrix.shape[0]

    index_perm = np.random.permutation(n_examples)

    perm_data_matrix = data_matrix[index_perm]
    perm_data_labels = data_labels[index_perm]

    return (perm_data_matrix, perm_data_labels)


# the merging of the representation can be done here

