
# NOTE: possibility of adding a and precompose compose option
class InvalidItemError(ValueError):
    pass

class bimapping:

    def __init__(self):
        self.direct = dict()
        self.inverse = dict()

    def __len__(self):
        return len(self.direct)

    # direct methods
    def direct_iteritems(self):
        return self.direct.iteritems()

    def direct_itervalues(self):
        return self.direct.itervalues()

    def direct_iterkeys(self):
        return self.direct.iterkeys()

    def direct_has_key(self, k):
        return self.direct.has_key(k)

    def direct_apply(self, keys, ignore=False, nodefault=False, default=None):
        values = []
        for k in keys:
            if k in self.direct:
                v = self.direct[k]
                values.append(v)
            else:
                if ignore:
                    if not nodefault:
                        values.append(default)
                else:
                    raise KeyError("Key %s not present in direct mapping." % (k, ))

        return values

    def direct_extend(self, items):
        for k, v in items:
            if k in self.direct or v in self.inverse:
                raise InvalidItemError("Item %s already exists partially or fully in the direct mapping." % ((k, v),))

            self.direct[k] = v
            self.inverse[v] = k

    def direct_remove(self, keys, ignore=False):
        for k in keys:
            if k in self.direct:
                v = self.direct[k]
                self.direct.pop(k)
                self.inverse.pop(v)
            else:
                if not ignore:
                    raise KeyError("Key %s does not exist in the direct mapping." % (k, ))

    # inverse methods
    def inverse_iteritems(self):
        return self.inverse.iteritems()

    def inverse_itervalues(self):
        return self.inverse.itervalues()

    def inverse_iterkeys(self):
        return self.inverse.iterkeys()

    def inverse_has_key(self, k):
        return self.inverse.has_key(k)

    def inverse_apply(self, keys, ignore=False, nodefault=False, default=None):
        values = []
        for k in keys:
            if k in self.inverse:
                v = self.inverse[k]
                values.append(v)
            else:
                if ignore:
                    if not nodefault:
                        values.append(default)
                else:
                    raise KeyError("Key %s not present in direct mapping." % (k, ))

        return values

    def inverse_extend(self, items):
        for k, v in items:
            if k in self.inverse or v in self.direct:
                raise InvalidItemError("Item %s already exists partially or fully in the inverse mapping." % ((k, v),))

            self.inverse[k] = v
            self.direct[v] = k

    def inverse_remove(self, keys, ignore=False):
        for k in keys:
            if k in self.inverse:
                v = self.inverse[k]
                self.inverse.pop(k)
                self.direct.pop(v)
            else:
                if not ignore:
                    raise KeyError("Key %s does not exist in the direct mapping." % (k, ))

