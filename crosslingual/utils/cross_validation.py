
import sklearn.cross_validation as cv
import numpy as np

# this is the signature of the functions that need to be passed
#fitted_model = fit_function(train_fold_matrix, train_fold_labels)
#fold_score = score_model(fitted_model, test_fold_matrix, test_fold_labels)

# this is completely general
def run_cross_validation(train_matrix, train_vector, fold_generator_function,
    fit_function, score_function):

    # for each fold of the training data
    score_list = list()
    for train_fold_index, test_fold_index in fold_generator_function():
        # train partition
        train_fold_matrix = train_matrix[train_fold_index]
        train_fold_vector = train_vector[train_fold_index]
        # test partition
        test_fold_matrix = train_matrix[test_fold_index]
        test_fold_vector = train_vector[test_fold_index]

        # train the model in training fold
        fitted_model = fit_function(train_fold_matrix, train_fold_vector)

        # score the fitted model on the test partition
        fold_score = score_function(fitted_model, test_fold_matrix, test_fold_vector)
        score_list.append(fold_score)

    return np.mean(score_list)


# NOTE in the score function, bigger is better
def get_best_regularization_parameter(train_matrix, train_vector,
    regularization_params_list, fold_generator_function, fit_function_factory,
    score_function):

    # best params for the model that we are training
    best_params = None
    best_score = - np.inf
    for reg_params in regularization_params_list:
        # NOTE that the parameters are a tuple
        fit_function = fit_function_factory(*reg_params)

        # compute the score according to cross validation
        score = run_cross_validation(train_matrix, train_vector,
            fold_generator_function, fit_function, score_function)

        if score > best_score:
            best_score = score
            best_params = reg_params

    return (best_params, best_score)


