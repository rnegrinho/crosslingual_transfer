#!/usr/bin/python

import numpy as np
import scipy.misc as mc

# NOTE that the computation of the cost function and of the gradient is
# not made in a numerically stable manner. This can lead to problems.
# NOTE assumes that class_list is a numpy array

def convert_from_labels_to_onehot(data_labels, class_list):
    N = len(data_labels)

    # create the design vector
    label_indices = map(list(class_list).index, data_labels)
    onehot_data_labels = np.zeros((N, len(class_list)), dtype='float')
    onehot_data_labels[xrange(N), label_indices] = 1.0

    return onehot_data_labels


def convert_from_onehot_to_labels(onehot_data_labels, class_list):
    assert onehot_data_labels.shape[1] == len(class_list), "The number of classes \
        and the number of columns of the one hot representation have to be the same."

    # create the design vector
    label_indices = onehot_data_labels.nonzero()[1]
    data_labels = class_list[label_indices]

    return np.array(data_labels)


# NOTE this has not been tested.
def evaluate_multiclass_logistic_cost(w, data_matrix, onehot_data_labels):
    N = data_matrix.shape[0]

    scores = data_matrix.dot(w)
    logsum_scores = mc.logsumexp(scores, axis=1)
    real_scores = scores[onehot_data_labels > 0]
    loss = - (1.0 / np.float(N)) * (np.sum(real_scores) - np.sum(logsum_scores))

    return loss


def compute_multiclass_gradient_logistic_cost(w, data_matrix, onehot_data_labels):
    N = data_matrix.shape[0]

    # these matrices are dense
    scores = data_matrix.dot(w)
    sum_scores = mc.logsumexp(scores, axis=1)
    norm_scores = np.exp(scores - sum_scores[:, None])
    aux = onehot_data_labels - norm_scores
    grad_w =  - (1 / float(N)) *  data_matrix.T.dot(aux)

    return grad_w



