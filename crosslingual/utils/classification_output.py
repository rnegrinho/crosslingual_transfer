
import collections
import pprint


# print the class distribution given the dictinary of examples
# change this to print dataset information
def print_class_label_distribution(dataset_dict):

    examples_per_class = {class_label : len(dataset_dict[class_label])
        for class_label in dataset_dict}

    total_examples = sum(examples_per_class.itervalues())

    frac_examples_per_class = \
        {class_label : examples_per_class[class_label] / float(total_examples)
        for class_label in dataset_dict}

    # print stuff
    print "Total number of examples: ", total_examples
    print "Examples per class"
    pprint.pprint(examples_per_class)
    print "Fraction of examples per class"
    pprint.pprint(frac_examples_per_class)


# print the class distribution given the dictinary of examples
def print_label_distribution(data_labels):
    # NOTE assumes a one dimensional array of labels

    labels_list = list(set(data_labels))

    examples_per_class = {label : sum(data_labels == label)
        for label in labels_list}

    total_examples = len(data_labels)

    frac_examples_per_class = \
        {label : examples_per_class[label] / float(total_examples)
        for label in labels_list}

    print "Total number of examples: ", total_examples
    print "Examples per class"
    pprint.pprint(examples_per_class)
    print "Fraction of examples per class"
    pprint.pprint(frac_examples_per_class)


# NOTE some of these functions assume that the labels in the arrays are the same
# some problems may arise if that is not the case. check what happens then.
def print_error_label_distribution(data_labels, pred_labels):

    zip_labels = zip(data_labels, pred_labels)
    label_pairs = collections.Counter(zip_labels)

    print "Total number of examples: ", len(data_labels)

    # getting the dictionary of predictions in a different format
    conf_dict = dict()
    uniq_labels = list(set(data_labels))
    for class_label in uniq_labels:
        conf_dict[class_label] = dict()
        for pred_label in uniq_labels:
            conf_dict[class_label][pred_label] = label_pairs[(class_label, pred_label)]

    print "Distribution of predictions per class (actual label --> {pred_labels})"
    pprint.pprint(conf_dict)

    # fraction of times for each class
    conf_dict_per_class = dict()
    for class_label in conf_dict:
        class_pred_dict = conf_dict[class_label]
        n_class_examples = sum(class_pred_dict.itervalues())

        conf_dict_per_class[class_label] = {c : float(count) / n_class_examples
            for (c, count) in class_pred_dict.iteritems()}

    print "Fraction of predictions per class"
    pprint.pprint(conf_dict_per_class)

    n_correct = sum(data_labels == pred_labels)
    n_examples = len(data_labels)
    print "Overall score: ", float(n_correct) / n_examples

