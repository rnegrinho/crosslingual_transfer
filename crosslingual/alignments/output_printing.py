

def print_processing_operation_information(tag, args):
    """
    Prints some information about the processing function specified by the tag
    and the arguments.
    """

    if tag == 'rare_alignments':
        print "Remove alignments that occur less than %d times" % args

    elif tag == 'rare_target':
        print "Remove target tokens that have less than %d alignments" % args

    elif tag == 'common':
        print "For each target token, keeps only up to the %d most frequent " \
            "alignments for that token" % args

    elif tag == 'percentile':
        print "For each target token, keeps alignments only up to the %0.2f " \
            "percentile" % args

    elif tag == 'unaligned':
        print "Remove the unaligned tokens (this is an auxiliary operation)"

    elif tag == 'normalize':
        print "For each target token, normalize the alignments counts to get " \
            "a probability distribution"

    else:
        raise Exception('Unrecognized function tag passed as argument.')


def print_information(info_dict):
    """
    Prints the information associated with the processing of the alignments.
    """

    print "** Information about the alignments"

    print "Path to the english to foreign alignments count file: ", \
        info_dict['english_to_foreign_alignments_filepath']

    print "Transfering in the reverse direction (foreign to english): ", \
        info_dict['reverse_transfer_direction']

    print "* Sequence of processing operations of the alignments matrix:"
    for t in info_dict['processing_taglist']:
        print_processing_operation_information(*t)

    print "* Information after processing"
    alignments_matrix = info_dict['alignments_matrix']
    print 'Number of target tokens: ', alignments_matrix.shape[0]
    print 'Number of source tokens: ', alignments_matrix.shape[1]
    print 'Number of nonzero alignments: ', alignments_matrix.nnz

    print "** End of information about the alignments"

