

def set_argument_parser(parser, prefix=''):

    parser.add_argument(prefix + 'idfs_filepath',
        help='path to the file with the idfs data',
        type=str)

    parser.add_argument(prefix + 'embeddings_filepath',
        help='path to the file with the embeddings data',
        type=str)

    # these are the optional arguments
    parser.add_argument('--' + prefix + 'emb_binarize',
        help='binarize the embeddings representation for the documents',
        action='store_true')

    parser.add_argument('--' + prefix + 'emb_normalize',
        help='normalize the embeddings representation for the documents by \
            dividing by the number of existing tokens',
        action='store_true')
