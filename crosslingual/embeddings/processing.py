

import numpy as np
import codecs


def read_embeddings_from_file(embeddings_filepath):

    # open the file with the example
    embeddings_file = codecs.open(embeddings_filepath, 'r', 'utf-8')

    embeddings_dict = dict()
    for line in embeddings_file:
        line = line.rstrip(' \n')

        splitted_line = line.split(' ')

        # get the token and the corresponding embedding vector
        token = splitted_line[0]
        embedding = np.array(splitted_line[1:], dtype=np.float)

        embeddings_dict[token] = embedding

    # close the file with the embeddings
    embeddings_file.close()

    return embeddings_dict


# NOTE the normalization can be done by the total number of words instead of
# just the ones that are in the embeddings dictionary
def transform_example(document_dict, idfs_dict, embeddings_dict, binarize,
    normalize):
    """
    Gets a document in the form of a dictionary, where each key is a token and
    the corresponding value is the number of times that token appears in the
    document. Also the dictionaries for the idfs and the embeddings are passed
    as input. The format for these two last dictionaries is the same as the
    document representation, i.e., the key is the token and the values are the
    corresponding information for the token key. Returns a representation for
    the document which is computed as the sum of the embeddings for each token
    in the document multiplied by its corresponding idf and the number of
    times it appears in the document. If there is no embedding or idf for a
    given token, that token is simply ignored. The size of the representation
    for the documents is determined by the size of the embeddings in the
    embeddings dictionary. All embeddings must have the same size.
    """

    # determine the size of the resulting document representation
    token_aux, embedding_aux = embeddings_dict.popitem()
    rep_shape = embedding_aux.shape
    embeddings_dict[token_aux] = embedding_aux

    # initialize the document that will have the representation in terms of
    # embeddings
    doc_representation = np.zeros(rep_shape, dtype='float')

    sum_counts = 0.0
    # for each token in the document
    for token in document_dict:

        # if there is a corresponding idf and embedding
        if token in idfs_dict and token in embeddings_dict:

            # sum the weighted embedding to the representation computed so far
            if binarize == True:
                count = 1.0
            else:
                count = document_dict[token]

            idf = idfs_dict[token]
            embedding = embeddings_dict[token]

            # ipdb.set_trace()
            doc_representation += count * idf * embedding
            sum_counts += np.float(count)

    # normalize according to the number of tokens in the dictionary
    if normalize == True:
        doc_representation /= np.float(sum_counts)

    #print sum_counts, int(sum(document_dict.itervalues()))

    return doc_representation



def transform_dataset(dataset_bow_dict, idfs_dict, embeddings_dict, binarize,
    normalize):
    """
    Transforms the whole dataset that is represented in the bag of words format
    as a dictionary to the embeddings representation.
    """
    sorted_class_label_list = dataset_bow_dict.keys()
    sorted_class_label_list.sort()

    example_list = list()
    label_list = list()
    for class_label in sorted_class_label_list:
        for example_bow_dict in dataset_bow_dict[class_label]:
            example_rep = transform_example(example_bow_dict, idfs_dict,
                embeddings_dict, binarize, normalize)

            example_list.append(example_rep)
            label_list.append(class_label)

    data_matrix = np.vstack(example_list)
    data_labels = np.array(label_list, dtype='str')

    return (data_matrix, data_labels)

if __name__ == '__main__':
    pass




