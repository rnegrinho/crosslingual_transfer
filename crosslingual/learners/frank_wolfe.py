
import sklearn.cross_validation as cv
import crosslingual.utils.cross_validation as cv_utils

# NOTE this is going to be specific of the each of the learners
def get_best_frank_wolfe_model(target_train_matrix, target_train_labels,
    regularization_params_list, fit_function_factory, score_function, number_of_folds):

    # function used to generate the folds
    def fold_generator_function():
        fold_generator = cv.StratifiedKFold(target_train_labels, number_of_folds)

        for fold in fold_generator:
            yield fold

    # get best regularization parameters
    best_params, best_score = cv_utils.get_best_regularization_parameter(target_train_matrix,
        target_train_labels, regularization_params_list, fold_generator_function,
        fit_function_factory, score_function)

    # get the best model using the best regularization parameter
    best_model = fit_function_factory(*best_params)(target_train_matrix,
        target_train_labels)

    # NOTE these print statements are not here to stay
    # NOTE the data can be passed out of this function in a output dictionary
    print "Best regularization parameter C found by cross validation:", best_params
    print "Average score for the %d folds: %0.4f" % (number_of_folds, best_score)
    # - best regularization parameter
    # - best score in cross validation
    # - number of nonzeros in the alignments matrix
    # - evaluation of each of the parts of the cost function
    ### TODO this is to be removed later, and use some sort of dictionary
    # instead

    return best_model

