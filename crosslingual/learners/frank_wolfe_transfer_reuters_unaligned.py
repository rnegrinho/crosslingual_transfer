
# TODO I don't know that the first one needs to be taken care of
import crosslingual.optimizers.frank_wolfe as fw_opt
import crosslingual.learners.frank_wolfe_transfer as fw_trans
import crosslingual.learners.frank_wolfe as fw_learn
import crosslingual.logistic.processing as log_proc
import crosslingual.utils.logistic as log_utils
import sklearn.cross_validation as cv
import crosslingual.utils.cross_validation as cv_utils
import scipy.sparse as sp
import numpy as np
import itertools

# auxiliary functions to split the state and merge it again
def split_w(w, split_index):
    return (w[:split_index], w[split_index:])

def join_w(w_align, w_unalign):
    return np.vstack((w_align, w_unalign))
#
# curried minimum atom function
def get_minimum_atom_factory(v, unaligned_reg_const):

    def minimum_atom_function(x_grad):

        full_w_grad, M_grad = x_grad

        M_atom = sp.dok_matrix(M_grad.shape, dtype='float')

        split_index = M_grad.shape[0]
        # I guess that the problem must be around here

        align_w_grad, unalign_w_grad = split_w(full_w_grad, split_index)

        assert align_w_grad.shape[0] == M_grad.shape[0]
        assert M_grad.shape[1] == v.shape[0]

        # NOTE maybe this can be improved using matrix methods directly
        # NOTE this part could be encapsulated as it is common to both
        # frank wolfe transfer methods
        for row_i in xrange(M_grad.shape[0]):
            # NOTE think of the things
            row = M_grad.getrow(row_i)

            _, column_indices, M_vals = sp.find(row)

            # this part comes from the outer product
            w_vals = align_w_grad[row_i, :].dot(v.T[:, column_indices])

            min_i = np.argmin(M_vals + w_vals)
            min_col_i = column_indices[min_i]

            M_atom[row_i, min_col_i] = 1.0

        # convert the resulting matrix to csr
        M_atom = M_atom.tocsr()

        ## getting all the atoms
        align_w_atom = M_atom.dot(v)

        # NOTE some care with the unaligned atom.
        # careful about a possible yet improbable, division by zero.
        norm_unalign_w_grad = np.linalg.norm(unalign_w_grad)
        if norm_unalign_w_grad > 0.0:
            unalign_w_atom = - (unaligned_reg_const / norm_unalign_w_grad) * unalign_w_grad
        else:
            unalign_w_atom = - unalign_w_grad

        full_w_atom = join_w(align_w_atom, unalign_w_atom)


        assert full_w_atom.shape == full_w_grad.shape

        x_atom = (full_w_atom, M_atom)

        return x_atom

    return minimum_atom_function


def fit_function_factory_parametrizer(alignments_matrix, cost_alignments_matrix,
        source_weight_vector, class_list, fw_number_of_steps):

    def fit_function_factory(aligned_reg_const, unaligned_reg_const):

        def fit_function(train_matrix, train_labels):

            # compute the one hot labels
            onehot_train_labels = log_utils.convert_from_labels_to_onehot(
                train_labels, class_list)

            # setiting up the functions used in the call to frank wolfe
            cost_function = fw_trans.get_cost_evaluation_factory(train_matrix,
                onehot_train_labels, cost_alignments_matrix)(aligned_reg_const)

            gradient_function = fw_trans.get_cost_gradient_factory(train_matrix,
                onehot_train_labels, cost_alignments_matrix)(aligned_reg_const)

            # NOTE this part is different from the simple case where nothing is
            # done with the unaligned words
            minimum_atom_function = get_minimum_atom_factory(source_weight_vector,
                    unaligned_reg_const)

            # starting the optimization from the simple transfer point
            transfered_weight_vector = alignments_matrix.dot(source_weight_vector)

            # concatenation of the aligned part with the unaligned part
            unaligned_size = train_matrix.shape[1] - alignments_matrix.shape[0]
            initial_unaligned_weight_vector = np.zeros((unaligned_size, transfered_weight_vector.shape[1]), dtype='float')

            #print transfered_weight_vector.shape
            #print initial_unaligned_weight_vector.shape
            #import ipdb
            #ipdb.set_trace()

            initial_weight_vector = np.vstack((transfered_weight_vector, initial_unaligned_weight_vector))

            assert initial_weight_vector.shape[0] == train_matrix.shape[1]

            x0 = (initial_weight_vector, alignments_matrix)

            # call the optimizer for the specified
            fw_weight_vector, fw_alignments_matrix = fw_opt.frank_wolfe_optimizer(x0,
                cost_function, gradient_function, fw_trans.update_function,
                minimum_atom_function, fw_trans.duality_gap_function, fw_number_of_steps)

            fw_alignments_matrix.eliminate_zeros()

            return (fw_weight_vector, fw_alignments_matrix)

        return fit_function

    return fit_function_factory

# this is the function that the runnable script has to call
def fit_model(target_train_matrix, target_train_labels, cost_alignments_matrix,
        alignments_matrix, source_weight_vector, class_list,
        lower_aligned_reg_const, upper_aligned_reg_const, number_aligned_reg_consts,
        lower_unaligned_reg_const, upper_unaligned_reg_const, number_unaligned_reg_consts,
        number_of_folds, fw_number_of_steps):


    # regularization params
    log_align_lower = np.log10(lower_aligned_reg_const)
    log_align_upper = np.log10(upper_aligned_reg_const)
    align_consts = np.logspace(log_align_lower, log_align_upper, number_aligned_reg_consts)

    log_unalign_lower = np.log10(lower_unaligned_reg_const)
    log_unalign_upper = np.log10(upper_unaligned_reg_const)
    unalign_consts = np.logspace(log_unalign_lower, log_unalign_upper, number_unaligned_reg_consts)

    regularization_params_list = itertools.product(align_consts, unalign_consts)

    # this is something that I need to develop here
    fit_function_factory = fit_function_factory_parametrizer(alignments_matrix,
            cost_alignments_matrix, source_weight_vector, class_list, fw_number_of_steps)

    score_function = fw_trans.score_function_factory(class_list)

    # call the learners, get best best frank wolfe model
    best_model = fw_learn.get_best_frank_wolfe_model(target_train_matrix,
            target_train_labels, regularization_params_list, fit_function_factory,
            score_function, number_of_folds)

    return best_model

