
import crosslingual.optimizers.frank_wolfe as fw_opt
import crosslingual.learners.frank_wolfe as fw_learn
import crosslingual.logistic.processing as log_proc
import crosslingual.utils.logistic as log_utils
import crosslingual.utils.cross_validation as cv_utils
import scipy.sparse as sp
import numpy as np

# curried cost function
def get_cost_evaluation_factory(data_matrix, onehot_data_labels, cost_alignments_matrix):

    def get_cost_evaluation_regularized_function(regularization_const):

        def cost_function(x):

            # unpack the tuple with elements
            w, M = x

            w_cost = log_utils.evaluate_multiclass_logistic_cost(w, data_matrix,
                onehot_data_labels)

            M_cost = regularization_const * M.multiply(cost_alignments_matrix).sum()

            return w_cost + M_cost

        return cost_function

    return get_cost_evaluation_regularized_function


# curried cost gradient function
def get_cost_gradient_factory(data_matrix, onehot_data_labels, cost_alignments_matrix):

    def get_cost_gradient_regularized_function(regularization_const):

        def gradient_function(x):

            # unpack the tuple with elements
            w, M = x

            w_grad = log_utils.compute_multiclass_gradient_logistic_cost(w,
                data_matrix, onehot_data_labels)

            M_grad = regularization_const * cost_alignments_matrix

            return (w_grad, M_grad)

        return gradient_function

    return get_cost_gradient_regularized_function


# computes the update given two tuple with the w and M components
def update_function(x, s, alpha):
    # this is done to preserve the sparsity pattern of the atoms
    cvx_comb = lambda x1, x2: (1.0 - alpha) * x1 + alpha * x2

    x_updated = tuple(map(cvx_comb, x, s))

    return x_updated


# curried minimum atom function
def get_minimum_atom_factory(v):

    def minimum_atom_function(x_grad):

        w_grad, M_grad = x_grad

        M_atom = sp.dok_matrix(M_grad.shape, dtype='float')

        # NOTE maybe this can be improved using matrix methods directly
        # I tried, but it did not help much.
        for row_i in xrange(M_grad.shape[0]):
            # NOTE think of the things
            row = M_grad.getrow(row_i)

            _, column_indices, M_vals = sp.find(row)

            # this part comes from the outer product
            w_vals = w_grad[row_i, :].dot(v.T[:, column_indices])

            min_i = np.argmin(M_vals + w_vals)
            min_col_i = column_indices[min_i]

            M_atom[row_i, min_col_i] = 1.0

        # convert the resulting matrix to csr
        M_atom = M_atom.tocsr()

        w_atom = M_atom.dot(v)
        x_atom = (w_atom, M_atom)

        return x_atom

    return minimum_atom_function


def duality_gap_function(x_grad, x, s):

    w_grad, M_grad = x_grad
    w, M = x
    w_s, M_s = s

    w_dgap = np.sum(np.multiply(w_grad, w - w_s))
    M_dgap = M_grad.multiply(M - M_s).sum()
    x_dgap = w_dgap + M_dgap

    return x_dgap


def fit_function_factory_parametrizer(alignments_matrix, cost_alignments_matrix,
        source_weight_vector, class_list, fw_number_of_steps):

    def fit_function_factory(align_reg_const):

        def fit_function(train_matrix, train_labels):

            # compute the one hot labels
            onehot_train_labels = log_utils.convert_from_labels_to_onehot(
                train_labels, class_list)

            # setiting up the functions used in the call to frank wolfe
            cost_function = get_cost_evaluation_factory(train_matrix,
                onehot_train_labels, cost_alignments_matrix)(align_reg_const)

            gradient_function = get_cost_gradient_factory(train_matrix,
                onehot_train_labels, cost_alignments_matrix)(align_reg_const)

            minimum_atom_function = get_minimum_atom_factory(source_weight_vector)

            # starting the optimization from the simple transfer point
            transfered_weight_vector = alignments_matrix.dot(source_weight_vector)
            x0 = (transfered_weight_vector, alignments_matrix)

            # call the optimizer for the specified
            fw_weight_vector, fw_alignments_matrix = fw_opt.frank_wolfe_optimizer(x0,
                cost_function, gradient_function, update_function,
                minimum_atom_function, duality_gap_function, fw_number_of_steps)

            fw_alignments_matrix.eliminate_zeros()

            return (fw_weight_vector, fw_alignments_matrix)

        return fit_function

    return fit_function_factory

# function scoring the model
def score_function_factory(class_list):
    def score_function(fitted_model, test_matrix, test_labels):

        weight_vector = fitted_model[0]

        return log_proc.score_model(weight_vector, class_list, test_matrix, test_labels)
    return score_function


# this is the function that the runnable script has to call
def fit_model(target_train_matrix, target_train_labels, cost_alignments_matrix,
        alignments_matrix, source_weight_vector, class_list,
        lower_aligned_reg_const, upper_aligned_reg_const, number_aligned_reg_consts,
        number_of_folds, fw_number_of_steps):

    # regularization params
    log_align_lower = np.log10(lower_aligned_reg_const)
    log_align_upper = np.log10(upper_aligned_reg_const)
    align_consts = np.logspace(log_align_lower, log_align_upper, number_aligned_reg_consts)

    regularization_params_list = zip(align_consts)

    fit_function_factory = fit_function_factory_parametrizer(alignments_matrix,
            cost_alignments_matrix, source_weight_vector, class_list, fw_number_of_steps)

    score_function = score_function_factory(class_list)

    # call the learners, get best best frank wolfe model
    best_model = fw_learn.get_best_frank_wolfe_model(target_train_matrix,
            target_train_labels, regularization_params_list, fit_function_factory,
            score_function, number_of_folds)

    return best_model


