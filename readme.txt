
This package contains code implementing some models for tackling the problem of 
crosslingual transfer in topic classification. This file contains a brief 
description of how to reproduce the experiments coded in this package.

We first begin by talking about the project folder structure:

** bin **

These are runnable scripts from the terminal. Currently they have python 
and bash scripts. Each python script either implements some specific model
(baseline or not) for the problem of crosslingual transfer or is a preprocessing
script, used to compute data from existing data. The python scripts implementing
the models take all the arguments to accomplish their task from the command line.
This allows to run a wide range of scenarios without modifying the source code. 
The bash scripts are used mostly to run the python scripts with a large number 
of different arguments. These are rather inflexible in purpose (e.g., running the 
model X on datasets D1, D2, and D3 varying some number of options). The scripts 
that run the models normally generate a log file that details the results 
attained. These logs are commonly outputted to the logs folder. Note that some 
of the bash scripts do require some source editing for running in some particular
way (e.g., for some specific dataset).

Check the command line arguments of the python scripts for an idea of what each
one does. To do this, one can simply run the python script without arguments, as
it will print the argument list for the script. 

Note that the scripts are mainly set for the german english europarl pair, but
by means of some adaptation, it is possible to get them working for other
language pairs without too much effort.

* files in bin

# compute idfs from europarl (in a few different ways) and reuters.
compute_idfs_europarl_flat.py, 
compute_idfs_reuters.py, 
compute_idfs_europarl.py
run_compute_idfs.sh 

# supervised logistic bow and embeddings models.
run_supervised_logistic_bow.py 
run_supervised_logistic_embeddings.py 

# crosslingual transfer models based on alignments and embeddings, respectively-
# the svd tries to use a low rank decomposition of the alignments matrix to do
# the transfer, but it did not seem to work out very well.
run_transfer_svd.py 
run_transfer_alignments.py 
run_transfer_embeddings.py 

# runs most of experiments, namely the supervised baselines (with and without idfs)
# and the transfer models using alignments and embeddings (with and without idfs, also).
# the frank wolfe transfer models are run in a bash separate file.
run_all_experiments.sh 

# runs the some experiments to test some particular embeddings.
run_test_embeddings.sh 

# these are the scripts implementing the semi supervised transfer of models.
# based on the frank wolfe algorithm.
run_transfer_frank_wolfe.py 
run_transfer_frank_wolfe_reuters_unaligned.py 
run_frank_wolfe_transfer.sh 

# some auxiliary processing scripts. the first one processes the output file 
# of the berkeley aligner. the second one is for preprocessing europarl for removing
# parallel lines that are potentially bad (i.e., next to an empty line).
process_berkeley_aligner_output.py 
remove_bad_lines.py 

# other scripts
test_idfs_performance.sh 
test_where_idfs_come_from.py 


** data **

Folder used for keeping the data used for running the models. This includes, 
the europarl language pair, the reuters dataset, the embeddings, and the idfs 
(and also possibly others). Most bash scripts refer to this folder for their
data.

For properly running the bash scripts, one must assure that the necessary files
are present in this folder. Look at the source of the script to check what  
files are needed. 


** logs ** 

Most bash scripts in bin output their information to files in this folder.
Check the source of those scripts for more information. This folder will 
contain mostly logs generated from the runs of the different scripts.


** crosslingual **

This folder contains the bulk of the code for implementing the models in the 
bin folder. These are mostly library functions developed for reading, processing,
and printing the data. We cover reading of data as idfs, embeddings,
alignments, and the reuters and europarl datasets. There are several functions to map 
between representations of data (e.g., from the raw data read from a file
to something more usable by the model as a data matrix). This latter part makes 
up a significant fraction of this package. There are also functions for
learning a logistic model (supervised setting) using cross-validation and 
functionality from scikit-learn. There are also some functions under utils for 
doing various things such as printing statistics for the predictions of a classifier,
computing idfs, and doing cross-validation for a model in a general way, not 
depending on any specific model that we are learning (we use higher-order 
functions for this). We have also some code implementing the Frank-Wolfe 
algorithm (again, in a general, abstract manner), and code for learning some 
of the Frank-Wolfe transfer models. Also, significantly important is the code
used to read the alignments file, generate a sparse matrix from them and 
preprocess it, as, for example, to remove rare alignments or rarely aligned tokens.

This package is probably is one of the most heterogeneous and, therefore, it
requires some understanding of the purpose of each of its parts to use 
effectively.


-----

IMPORTANT NOTE: All scripts (those in the bin folder included, both python and bash) 
must be run from the top directory, i.e., crosslingual/.. . This is 
required for proper recognition of the crosslingual package by the scripts.
It may also be necessary to run something in the terminal akin to: 
export PYTHONPATH=/absolute/path/to/crosslingual/package (or simply export
PYTHONPATH=. if one is currently in the mentioned path). If one does not run 
such commands, the files will not be recognized. We follow with an 
illustrative example: suppose that the path to the project is 
/home/renato/crosslingual_transfer and that we want to run the script  
/home/renato/crosslingual_transfer/bin/x.sh , then it must be called from 
/home/renato/crosslingual_transfer as ./bin/x.sh . 

When hoping to run the scripts in this package, one must first guarantee that 
all data files are in place with the expected names. These include europarl 
paralell files (although we mostly use the alignments obtained from this corpus); 
the file with the alignments statistics where each line is a three tuple with 
a pair of language1, language2 tokens and the number of times these tokens 
have been aligned together; the reuters dataset with the expected folder
structure (the folder structure is the same as Titov's for the same dataset); 
the embeddings in the predefined format; finally, there are also some files
with idfs required to run some of the experiments. 

As said previously, most of the bash are done for the german-english pair only,
but they can be adapted without much effort for other pairs if one is fairly
familiar with the code's logic. Note that this also requires a reuters dataset for
the specific language pair for evaluating the transfer part.

The models approached in this work can be used to transfer bag-of-words models
between languages. The problem of document classification was just a convenient
applicational example, but any problem that uses a bag-of-words representation
and has access to the data in one language but not in some other desired language
(or very little data in the desired language) where we what to learn our model, 
can make use of such an approach.

In case the data in the processed format are not available (i.e., a folder with 
the data files was not provided to you), you can track the following steps to
get a working system:

- Download the europarl corpus for the german-english pair (http://www.statmt.org/europarl/)
- Download the reuters corpus (https://www.dropbox.com/s/4prj3itybhnzxc8/document-representations.tar.gz).

Be careful as the dataset downloaded here has a problem that needs to be
solved before using it. There are a lot of hidden binary files
along with the data. This is troublesome because in some scripts, all files in a
given folder are read and therefore, these (garbage) binary files are treated as
proper data. To view the list of hidden files in the folder do:
ls -A -R document-representations | grep '^\.' | less 
and to remove all the hidden files do:
ls -A -R document-representations | grep '^\.' | xargs rm 
. All this is done after extracting the zip, obviously.

- Download the titov embeddings (http://klementiev.org/data/distrib/)
- Download the tools used for tokenizing(http://www.statmt.org/europarl/v7/tools.tgz)
- Download the code for training the blunsom embeddings (https://github.com/karlmoritz/bicvm)
- Download the berkeley aligner (https://code.google.com/p/berkeleyaligner/downloads/list)

After downloading all these files, one still has to do more processing for 
getting things in the proper format.

- Remove empty lines from the paralell corpus using the remove_bad_lines.py script.
One can just remove the pairs of sentences where at least one is empty or 
can consider a larger neighbourhood around the empty sentences for removal.

- Tokenize the corpus using the europarl tools that were downloaded.
- Lowercase the corpus. Be careful about the unicode support of the tools that 
you are using for this effect (sed works, I think).

- Run the berkeley aligner for the europarl (tokenized and lowercased) corpus with 
the europarl.conf file that is the data directory. This will generate an 
output file called training.align. This will take a while.

- Use the script process_berkeley_aligner_output.py to process the training.align
file. This generates a file where each row represents the number of times two 
tokens were aligned together.

- Train the blunsom embeddings. Use the processed europarl corpus (empty lines 
removed, tokenized, lowercased).

- Compute some idfs from the reuters corpus. There are some idfs inside the zip
along with the reuters corpus (although these are not in the correct format). 
To compute these idfs, one can use the compute_idfs_reuters.py script. There are 
also some, so called, unweighted idfs required for running the bash scripts.
These are just a consequence of the poor design decision of making ifds mandatory.
An unweighted idfs file is just like a normal idfs file, but where the idfs 
carry no information (they are all equal to one). The unix text processing tools 
are useful for generating these files from actual idfs files.

After following all these steps, all data required to run experiments is 
available. Note that for running the bash scripts, if may be required to 
change the source for updating the paths to some files.

